



/*username=spz0001 password=spz*/;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `attendance`
--

 
-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: attendance
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `emp_id` int(20) NOT NULL AUTO_INCREMENT,
  `id_code` varchar(20) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `father_name` varchar(50) DEFAULT NULL,
  `department_id` varchar(50) NOT NULL,
  `designation_id` varchar(50) NOT NULL,
  `dob` date DEFAULT NULL,
  `doj` date DEFAULT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `permanent_address` varchar(500) DEFAULT NULL,
  `phone_number` int(50) DEFAULT NULL,
  `relation_name` varchar(50) DEFAULT NULL,
  `relationship` varchar(50) DEFAULT NULL,
  `relation_contactnumber` int(50) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `active_tilldate` date DEFAULT NULL,
  `isadmin` tinyint(4) NOT NULL DEFAULT '0',
  `isuser` tinyint(4) NOT NULL DEFAULT '1',
  `image` blob NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--


/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'spz0001','admin','admin','spowerz','1','1','2021-09-15','2021-09-15','spowerz@gmail.com','spowerz','spowerz',123456789,'','',123456789,1,'2020-09-01',1,1,'');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-15 11:48:49

--
-- Table structure for table `designationmaster`
--

DROP TABLE IF EXISTS `designationmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designationmaster` (
  `designation_id` int(10) NOT NULL AUTO_INCREMENT,
  `designation_name` varchar(50) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `priority` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`designation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employee_progress`
--

DROP TABLE IF EXISTS `employee_progress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_progress` (
  `emp_id` int(20) NOT NULL,
  `progress_id` int(20) NOT NULL,
  `currentactive` tinyint(4) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `shift_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `account_number` varchar(50) DEFAULT NULL,
  `grosssalary` decimal(20,2) DEFAULT NULL,
  `basic` decimal(20,2) DEFAULT NULL,
  `da` decimal(20,2) DEFAULT NULL,
  `hra` decimal(20,2) DEFAULT NULL,
  `conveyance` decimal(20,2) DEFAULT NULL,
  `medicalallowance` decimal(20,2) DEFAULT NULL,
  `specialallowance` decimal(20,2) DEFAULT NULL,
  `otherallowances` decimal(20,2) DEFAULT NULL,
  `emp_salary_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employee_salary`
--

DROP TABLE IF EXISTS `employee_salary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_salary` (
  `emp_id` int(11) NOT NULL,
  `emp_salary_id` int(11) NOT NULL AUTO_INCREMENT,
  `progress_id` int(11) NOT NULL,
  `grosssalary` decimal(20,2) DEFAULT NULL,
  `basic` decimal(20,2) DEFAULT NULL,
  `da` decimal(20,2) DEFAULT NULL,
  `hra` decimal(20,2) DEFAULT NULL,
  `conveyance` decimal(20,2) DEFAULT NULL,
  `medicalallowance` decimal(20,2) DEFAULT NULL,
  `specialallowance` decimal(20,2) DEFAULT NULL,
  `otherallowances` decimal(20,2) DEFAULT NULL,
  `department_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `account_number` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`emp_salary_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

 
--
-- Table structure for table `employeeattendance`
--

DROP TABLE IF EXISTS `employeeattendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employeeattendance` (
  `attendance_id` int(10) NOT NULL,
  `emp_id` int(10) NOT NULL,
  `attendance_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `signin` time DEFAULT NULL,
  `signout` time DEFAULT NULL,
  `permission_from` time DEFAULT NULL,
  `permission_to` time DEFAULT NULL,
  `permission_remarks` varchar(50) DEFAULT NULL,
  `onduty_from` time DEFAULT NULL,
  `onduty_to` time DEFAULT NULL,
  `onduty_remarks` varchar(50) DEFAULT NULL,
  `permission_count` int(10) DEFAULT NULL,
  `leave_count` int(10) DEFAULT NULL,
  `working_hours` int(10) DEFAULT NULL,
  `lattitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `menuuserrights`
--

DROP TABLE IF EXISTS `menuuserrights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menuuserrights` (
  `menuid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `isadmin` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `usermaster`
--

--
-- Table structure for table `usermaster`
--

DROP TABLE IF EXISTS `usermaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usermaster` (
  `userid` int(10) NOT NULL AUTO_INCREMENT,
  `passwd` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `emp_id` varchar(20) NOT NULL,
  `defaultlandingpage` varchar(200) DEFAULT 'dashboard/index.php',
  `usernameid` varchar(50) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usermaster`
--


/*!40000 ALTER TABLE `usermaster` DISABLE KEYS */;
INSERT INTO `usermaster` VALUES  (1,'ú±X>‘ $|æ-&I;ªtŠ','1','/attendancemaster/pages/master/employee.php','spz0001');


--
-- Table structure for table `userprivileges`
--

DROP TABLE IF EXISTS `userprivileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userprivileges` (
  `accessid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `menuid` int(11) NOT NULL,
  `viewdetails` tinyint(4) NOT NULL DEFAULT '0',
  `editdetails` tinyint(4) NOT NULL DEFAULT '0',
  `addmenu` tinyint(4) NOT NULL DEFAULT '0',
  `deletemenu` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`accessid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `bank_master`
--

CREATE TABLE `bank_master` (
  `bank_id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `branch_name` varchar(50) DEFAULT NULL,
  `ifsc_code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Table structure for table `departmentmaster`
--

CREATE TABLE `departmentmaster` (
  `department_id` int(10) NOT NULL,
  `department_name` varchar(30) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `priority` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Table structure for table `menumaster`
--

CREATE TABLE `menumaster` (
  `menuid` int(11) NOT NULL,
  `menuname` varchar(100) DEFAULT NULL,
  `menuurl` varchar(200) DEFAULT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT 1,
  `menupath` varchar(200) DEFAULT NULL,
  `menuheader` varchar(50) DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `displayicon` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Table structure for table `notificationtype_master`
--

CREATE TABLE `notificationtype_master` (
  `notificationtypeid` int(11) NOT NULL,
  `notificationname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Table structure for table `notification_users_21`
--

CREATE TABLE `notification_users_21` (
  `userid` int(11) NOT NULL,
  `circularid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Table structure for table `shift_hours`
--

CREATE TABLE `shift_hours` (
  `shift_no` tinyint(4) NOT NULL,
  `hourno` tinyint(4) NOT NULL,
  `nextshift` tinyint(4) DEFAULT NULL,
  `previousshift` tinyint(4) DEFAULT NULL,
  `nexthour` tinyint(4) DEFAULT NULL,
  `previoushour` tinyint(4) DEFAULT NULL,
  `starthour` varchar(8) DEFAULT NULL,
  `endhour` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Table structure for table `shift_master`
--

CREATE TABLE `shift_master` (
  `shift_id` int(11) NOT NULL,
  `shift_no` int(11) NOT NULL,
  `shift_name` varchar(50) DEFAULT NULL,
  `shift_start_time` varchar(20) NOT NULL DEFAULT '',
  `shift_end_time` varchar(20) NOT NULL DEFAULT '',
  `isactive` tinyint(4) NOT NULL DEFAULT 1,
  `priority` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank_master`
--
ALTER TABLE `bank_master`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `departmentmaster`
--
ALTER TABLE `departmentmaster`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `menumaster`
--
ALTER TABLE `menumaster`
  ADD PRIMARY KEY (`menuid`);

--
-- Indexes for table `notificationtype_master`
--
ALTER TABLE `notificationtype_master`
  ADD PRIMARY KEY (`notificationtypeid`);

--
-- Indexes for table `shift_hours`
--
ALTER TABLE `shift_hours`
  ADD PRIMARY KEY (`shift_no`,`hourno`);

--
-- Indexes for table `shift_master`
--
ALTER TABLE `shift_master`
  ADD PRIMARY KEY (`shift_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank_master`
--
ALTER TABLE `bank_master`
  MODIFY `bank_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departmentmaster`
--
ALTER TABLE `departmentmaster`
  MODIFY `department_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menumaster`
--
ALTER TABLE `menumaster`
  MODIFY `menuid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notificationtype_master`
--
ALTER TABLE `notificationtype_master`
  MODIFY `notificationtypeid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shift_master`
--
ALTER TABLE `shift_master`
  MODIFY `shift_id` int(11) NOT NULL AUTO_INCREMENT;


