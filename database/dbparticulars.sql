create schema attendancemaster;

DROP TABLE IF EXISTS `dbparticulars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbparticulars` (
  `dbname` varchar(15) NOT NULL,
  `clientname` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbparticulars`
--

/*!40000 ALTER TABLE `dbparticulars` DISABLE KEYS */;
INSERT INTO `dbparticulars` VALUES ('attendance','spz');
/*!40000 ALTER TABLE `dbparticulars` ENABLE KEYS */;
