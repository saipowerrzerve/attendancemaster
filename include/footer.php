<!-- <?php
      $footermessageqry = "select * from emsmaster.clientparticulars where databasename = '" . $_POST['frmclientid'] . "'";
      $footerresult = mysqli_query($dbh, $footermessageqry);
      $footerrow = mysqli_fetch_array($footerresult);
      $footermessage = $footerrow['footermessage'];
      ?> -->
<footer class="main-footer">
  <div class="float-right d-none d-sm-block">
    <b>SPZ</b> 1.0
  </div>
  <strong> <?php echo $footermessage; ?> </strong> [ <a href="http://www.spowerz.com">SPZ</a> <a href="http://www.winstargroup.org">WinStar</a>]
  reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Bootstrap -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jQuery UI -->
<script src="../../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- SweetAlert2 -->
<script src="../../plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="../../plugins/toastr/toastr.min.js"></script>
<!-- Moment -->
<script src="../../plugins/moment/moment.min.js"></script>
<!-- date-range-picker -->
<script src="../../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Inputmask -->
<script src="../../plugins/inputmask/jquery.inputmask.min.js"></script>

<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<!-- dropzonejs -->
<script src="../../plugins/dropzone/min/dropzone.min.js"></script>
<!-- Page specific script -->

<script>
  topframe();
  //Date picker
  $('.datedmy').datetimepicker({
    timepicker: false,
    format: 'DD/MM/YYYY'
  });

  $('.datelt').datetimepicker({
    datepicker: false,
    format: 'LT'
  })
  //------------------------SWEET ALERT---------------------------------------------//
  function statusswal(message, type) {
    Swal.fire("", message, type);
  }
  //------------------------TOAST ALERT---------------------------------------------//
  function statustoast(message, type) {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    Toast.fire({
      icon: type,
      title: type
    })
  }
  //------------------------LOADER--------------------------------------------------//
  function loaderstart(divclass, loaderclass) {
    $(".loaderclass").show().removeAttr('hidden');
    $(".loaderclass").html(' <div class="d-flex justify-content-center"> <div class="spinner-border text-primary" role="status"> <span class="sr-only">Loading...</span> </div></div>');

    $(".divclass").hide();

  }

  function loaderend(divclass, loaderclass) {
    $(".loaderclass").attr('hidden', 'hidden');
    $(".divclass").show();
  }
  //-------------------------select2----------------------------------------------//
  //Initialize Select2 Elements
  $('.select2').select2()

  //Initialize Select2 Elements
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })
  //--------------------------inputmask--------------------------------------------//
  $(".salary").inputmask({
    mask: "9999999[.999]",
    greedy: false,
    definitions: {
      '*': {
        validator: "[0-9]"
      }
    },
    //rightAlign: true
  });
  //--------------------------logoff-----------------------------------------------//
  function logoff() {
    Swal.fire({
      title: 'Are you sure want to logoff?',
      //text: "You won't be able to revert this!",
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Logoff'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = "../../index.php?db=<?php echo $db; ?>";
      }
    })
  }
  //-----------------------------dropzone---------------------------------------//
  // DropzoneJS Demo Code Start
  Dropzone.autoDiscover = false

  // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
  var previewNode = document.querySelector("#template")

  if (previewNode) {
    previewNode.id = ""
    var previewTemplate = previewNode.parentNode.innerHTML
    previewNode.parentNode.removeChild(previewNode)
    var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
      url: "imageupload.php", // Set the url
      maxFilesize: 2, // MB
      thumbnailWidth: 150,
      thumbnailHeight: 150,
      parallelUploads: 20,
      previewTemplate: previewTemplate,
      autoQueue: false, // Make sure the files aren't queued until manually added
      previewsContainer: "#previews", // Define the container to display the previews
      clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files.
    })
    myDropzone.on("addedfile", function(file) {
      // Hookup the start button
      file.previewElement.querySelector(".start").onclick = function() {
        myDropzone.enqueueFile(file)
      }
    })
    document.querySelector("#actions .cancel").onclick = function() {
      myDropzone.removeAllFiles(true)
    }
    document.querySelector(".fileinput-button").onclick = function() {
      myDropzone.removeAllFiles(true)
    }
  }
  // DropzoneJS Demo Code End

  function topframe()
  {
    var topframeuserid=$('#topframeuserid').val()
    $.ajax({
            type: "POST",
            url: "../../include/topframedetails.php",
            data:{
              topframeuserid_dt: topframeuserid
            },
            success: function(response) {
              //$("#drpval").append(response);
              alert(response)
            }
        })
  }
</script>

</body>

</html>