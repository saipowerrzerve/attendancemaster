<!DOCTYPE html>
<?php
error_reporting(E_ALL ^ E_NOTICE);
// if (!isset($_POST['topframedatabase'])) {echo "<br><br><b><center>Cannot access this screen directly from Bookmarks!
//  Please login from homepage to gain access!</b></center>"; exit;} 
?>
<?php

if (isset($_GET['displaypage'])) {
  $currentdisplaypage = $_GET['displaypage'];
} else {
  $currentdisplaypage = 1;
}
// echo $_POST['topframedatabase']
?>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/gif/png" href="../../images/favicon.ico">
  <style>
    .lay {
      background-image: linear-gradient(15deg, #13547a 0%, #80d0c7 100%);
    }

    .pulse {
      animation: pulse-animation 1s infinite;
    }

    @keyframes pulse-animation {
      0% {
        box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.2);
      }

      100% {
        box-shadow: 0 0 0 20px rgba(0, 0, 0, 0);
      }
    }


    .master {
      color: black;
      float: right;
    }

    @media (max-width:900px) {
      .master {
        display: none
      }
    }
  </style>
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="../../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="../../plugins/toastr/toastr.min.css">

  <!-- daterange picker -->
  <link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="../../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

  <!-- Select2 -->
  <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">

  <!-- dropzonejs -->
  <link rel="stylesheet" href="../../plugins/dropzone/min/dropzone.min.css">

  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
</head>
<form name="frmtopframe">
  <input type="hidden" value="<?php echo $_POST['topframedatabase']; ?>"id="topframedatabase" name="topframedatabase">
  <input type="hidden" value="<?php echo $_POST['topframeusername']; ?>"id="topframeusername" name="topframeusername">
  <input type="hidden" value="<?php echo $_POST['topframeuserid']; ?>"id="topframeuserid" name="topframeuserid">
</form>

<body class="sidebar-collapse">
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand  lay navbar-dark ">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a href="https://spowerz.business.site/" class="dropdown-item"><img src="../../images/logo.jpg" class="img" alt="User Image"></a>

        </li>
        <!--
        <li class="nav-item d-none d-sm-inline-block">
          <a href="../../index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="#" class="nav-link">Contact</a>
        </li> -->
        <li class="nav-item dropdown">
          <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fas fa-address-book"></i> Master</a>
          <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow" id="drpval">
            <!-- <li><a href="employee.php" class="dropdown-item">Employee</a></li>
            <li class="dropdown-divider"></li>
            <li><a href="departmentmaster.php" class="dropdown-item">Department</a></li>
            <li class="dropdown-divider"></li>
            <li><a href="designationmaster.php" class="dropdown-item">Desigination</a></li>
            <li class="dropdown-divider"></li>
            <li><a href="bankmaster.php" class="dropdown-item">Bank</a></li>
            <li class="dropdown-divider"></li>
            <li><a href="shiftmaster.php" class="dropdown-item">Shift</a></li>
            <li class="dropdown-divider"></li>
            <li><a href="shifthours.php" class="dropdown-item">Shift Hours</a></li>
            <li class="dropdown-divider"></li>
            <li><a href="usermaster.php" class="dropdown-item">User</a></li>
            <li class="dropdown-divider"></li>  
            <li><a href="employee_salary.php" class="dropdown-item">Salary</a></li>
            <li class="dropdown-divider"></li> -->
            
            
            <!-- Level two dropdown-->
            <!-- <li class="dropdown-submenu dropdown-hover">
              <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Hover for action</a>
              <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                <li>
                  <a tabindex="-1" href="#" class="dropdown-item">level 2</a>
                </li> -->

            <!-- Level three dropdown-->
            <!-- <li class="dropdown-submenu">
                  <a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">level 2</a>
                  <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                    <li><a href="#" class="dropdown-item">3rd level</a></li>
                    <li><a href="#" class="dropdown-item">3rd level</a></li>
                  </ul>
                </li> -->
            <!-- End Level three -->

            <!-- <li><a href="#" class="dropdown-item">level 2</a></li>
                <li><a href="#" class="dropdown-item">level 2</a></li>
              </ul>
            </li> -->
            <!-- End Level two -->
          </ul>
        </li>
        <!-- <li class="nav-item dropdown">
          <a id="dropdownSubMenu2" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fas fa-user-plus"></i>Menu</a>
          <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
            <li><a href="menumaster.php" class="dropdown-item">Menu</a></li>
            <li class="dropdown-divider"></li>
            <li><a href="menuuserrights.php" class="dropdown-item">Menu rights</a></li>
            <li class="dropdown-divider"></li>
            <li><a href="userprivileges.php" class="dropdown-item">Userprivileges</a></li>
            <li class="dropdown-divider"></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a id="dropdownSubMenu3" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="far fa-envelope"></i> Notification</a>
          <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
            <li><a href="notificationusers.php" class="dropdown-item">notificationusers</a></li>
            <li class="dropdown-divider"></li>
            <li><a href="notificationtypemaster.php" class="dropdown-item">notificationtype</a></li>
            <li class="dropdown-divider"></li>
          </ul>
        </li> -->
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown pulse">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
          </a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link" onclick="logoff()" href="#" role="button">
            <i class="fas fa-users"></i>
          </a>
        </li> -->
        <li class="nav-item">
          <div class="user-block" onclick="logoff()">
            <img class="img-circle img-bordered-sm" src="../../images/logo.jpg" alt="User Image">
          </div>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->