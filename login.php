<?php
session_start();
session_destroy();

?>
<html>

<head>
	<style>
		/* @import url('https://fonts.googleapis.com/css?family=Raleway:400,700'); */

		* {
			box-sizing: border-box;
			margin: 0;
			padding: 0;
			font-family: Raleway, sans-serif;
		}

		body {
			background: linear-gradient(90deg, #C7C5F4, #776BCC);
		}

		.container {
			display: flex;
			align-items: center;
			justify-content: center;
			min-height: 100vh;
		}

		.screen {
			background: linear-gradient(90deg, #5D54A4, #7C78B8);
			position: relative;
			height: 600px;
			width: 360px;
			box-shadow: 0px 0px 24px #5C5696;
		}

		.screen__content {
			z-index: 1;
			position: relative;
			height: 100%;
		}

		.screen__background {
			position: absolute;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			z-index: 0;
			-webkit-clip-path: inset(0 0 0 0);
			clip-path: inset(0 0 0 0);
		}

		.screen__background__shape {
			transform: rotate(45deg);
			position: absolute;
		}

		.screen__background__shape1 {
			height: 520px;
			width: 520px;
			background: #FFF;
			top: -50px;
			right: 120px;
			border-radius: 0 72px 0 0;
		}

		.screen__background__shape2 {
			height: 220px;
			width: 220px;
			background: #6C63AC;
			top: -172px;
			right: 0;
			border-radius: 32px;
		}

		.screen__background__shape3 {
			height: 540px;
			width: 190px;
			background: linear-gradient(270deg, #5D54A4, #6A679E);
			top: -24px;
			right: 0;
			border-radius: 32px;
		}

		/* .screen__background__shape4 {
	height: 400px;
	width: 200px;
	background: #7E7BB9;	
	top: 420px;
	right: 50px;	
	border-radius: 60px;
} */

		.login {
			width: 250px;
			padding: 30px;
			padding-top: 56px;
		}

		.login__field {
			padding: 20px 0px;
			position: relative;
		}

		.login__icon {
			position: absolute;
			top: 30px;
			color: #7875B5;
		}

		.login__input {
			border: none;
			border-bottom: 2px solid #D1D1D4;
			background: none;
			padding: 10px;
			padding-left: 24px;
			font-weight: 700;
			width: 105%;
			transition: .2s;
		}

		.login__input:active,
		.login__input:focus,
		.login__input:hover {
			outline: none;
			border-bottom-color: #6A679E;
		}

		.login__submit {
			background: #fff;
			font-size: 14px;
			margin-top: 30px;
			padding: 16px 20px;
			border-radius: 26px;
			border: 1px solid #D4D3E8;
			text-transform: uppercase;
			font-weight: 700;
			display: flex;
			align-items: center;
			width: 100%;
			color: #4C489D;
			box-shadow: 0px 2px 2px #5C5696;
			cursor: pointer;
			transition: .2s;
		}

		.login__submit:active,
		.login__submit:focus,
		.login__submit:hover {
			border-color: #6A679E;
			outline: none;
		}

		.button__icon {
			font-size: 24px;
			margin-left: auto;
			color: #7875B5;
		}

		.social-login {
			position: absolute;
			height: 140px;
			width: 200px;
			text-align: center;
			bottom: 0px;
			right: 0px;
			color: #fff;
		}

		.social-icons {
			display: flex;
			align-items: center;
			justify-content: center;
		}

		.social-login__icon {
			padding: 20px 10px;
			color: #fff;
			text-decoration: none;
			text-shadow: 0px 0px 8px #7875B5;
		}

		.social-login__icon:hover {
			transform: scale(1.5);
		}

		.hdtxt {
			text-transform: uppercase;
			/* linear-gradient(90deg, #5D54A4, #7C78B8) */
			background: linear-gradient(to right, #5D54A4 0%, #7C78B8 100%);
			-webkit-background-clip: text;
			-webkit-text-fill-color: transparent;
			font-size: 3vw;
			font-family: 'Poppins', sans-serif;
		}

		;
	</style>



	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SPZ - PowerZ EnMS</title>
	<link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="plugins/select2/css/select2.min.css">
	<link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="dist/css/adminlte.min.css">
	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
	<!-- Toastr -->
	<link rel="stylesheet" href="plugins/toastr/toastr.min.css">

	<!-- jQuery -->
	<script src="plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Select2 -->
	<script src="plugins/select2/js/select2.full.min.js"></script>

	<!-- SweetAlert2 -->
	<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
	<!-- Toastr -->
	<script src="plugins/toastr/toastr.min.js"></script>

	<script>
		$(function() {
			//Initialize Select2 Elements
			$('.select2').select2()

			//Initialize Select2 Elements
			$('.select2bs4').select2({
				theme: 'bootstrap4'
			})

		})
	</script>
	<script type="text/javascript">
		function generatepassword() {
			with(window.document.frmindex) {
				method = "post";
				action = "index.php?generatepwd=Y";
				submit();
			}
		}
		//----------------------------------Toast alert start------------------------------
		function Toast_alert(message, type) {
			toastr.options = {
				"closeButton": false,
				"progressBar": true,
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "3000",
				"timeOut": "3000",
				"extendedTimeOut": "1000",
				"showMethod": "slideDown",
				"hideMethod": "slideUp"
			}
			switch (type) {
				case "info":
					toastr.info('Incorrect User Name / Password, Please check username and password.')
					break;
				case "success":
					toastr.success('Incorrect User Name / Password, Please check username and password.')
					break;
				case "warning":
					toastr.warning('Incorrect User Name / Password, Please check username and password.')
					break;
				case "error":
					toastr.error('Incorrect User Name / Password, Please check username and password.')
					break;
			}
		}
		//----------------------------------Toast alert end--------------------------------------
	</script>

</head>

<body>
	<div class="container">
		<div class="screen">
			<div class="screen__content">

				<form method="post" class="login" action="logincheck.php<?php if (isset($_GET['customer'])) { ?>?customer=<?php echo $_GET['customer'];
																														} ?>">
					<?php if (isset($_GET['err'])) {
						echo '<script type="text/javascript">	Toast_alert("Incorrect User Name / Password - Click Generate Password to get new Credentials", "warning")</script>';
					} ?>
					<?php
					if (isset($_GET['db']) && $_GET['db'] != "") {
						$curdb = $_GET['db'];
						$dbname = "'" . $_GET['db'] . "', ";
					} else {
						$curdb = "";
						$dbname = "";
					}
					if ($dbname == "") {
						$domainqry = "SELECT clientname, dbname FROM attendancemaster.dbparticulars ";
						if ($servername != "") {
							$domainqry .= " where clientname = '$servername'";
						}
						//echo $domainqry;
						$domainresult = mysqli_query($dbh, $domainqry);
						$selectedlogo = "";
						$dbname = "";
						//echo $dbname;
						while ($domainrow = mysqli_fetch_array($domainresult)) {
							$dbname .= "'" . $domainrow['dbname'] . "', ";
						}
					}
					if ($dbname == "") {
						$sqlquery = "SELECT clientname, dbname FROM attendancemaster.dbparticulars  order by clientname";
					} else if (isset($_GET['customer'])) {
						$dbname = strrev(substr(strrev($dbname), 2, strlen($dbname)));
						$sqlquery = "SELECT clientname, dbname FROM attendancemaster.dbparticulars where dbname in ($dbname)  order by clientname";
					} else {
						$dbname = strrev(substr(strrev($dbname), 2, strlen($dbname)));
						$sqlquery = "SELECT clientname, dbname FROM attendancemaster.dbparticulars where dbname in ($dbname) order by clientname";
					}
					//echo $sqlquery;
					//exit;
					$result = mysqli_query($dbh, $sqlquery); ?>
					<div class="row form-group">
						<div class="col-sm-12">
							<?php // echo $dbname; 
							?>
							<?php if (isset($_GET['customer'])) {
								$customerid = $_GET['customer'];
								// $selectedlogo = "sbdmlogo.jpg"; 
							?>
								<?php if ($curdb == "") {
									echo "Unauthorized Access! Please contact Administrator";
								} else { ?>
									<input type="hidden" name="topframedatabase" value="<?php echo $curdb; ?>">
									<input type="hidden" name="topframecustomer" value="<?php echo $_GET['customer']; ?>">
									<!-- <img id="srclogotn" src="images/<?php echo $curdb; ?>/<?php echo $customerid; ?>/logo.jpg"> -->
								<?php } ?>
							<?php } else { ?>
								<h6 class="hdtxt"> POWERZ</h6>
								<select title="Select Client" class="form-control select2" name="topframedatabase" onchange="changelogo()" id="dbselect">
									<?php
									$rowcount = 0;
									$firstdb = 1;
									$selectedclientcode = "";
									if ($dbname != "") {
										$selectedclientcode = str_replace("'", "", $dbname);
										//echo $selectedclientcode;
									}
									$result = mysqli_query($dbh, $sqlquery);
									while ($row = mysqli_fetch_array($result)) {
										$rowcount++;
										$clientcode = $row['dbname'];
										$clientname = $row['clientname'];
									?>
										<option value="<?php echo $clientcode; ?>|<?php echo $selectedlogo; ?>" data-thumbnail="images/<?php echo $clientcode; ?>/tn-logo.jpg" <?php if ($selectedclientcode == $clientcode) { ?> selected<?php } ?>><?php echo $clientname; ?></option>
									<?php } ?>
								</select>
								<script language="javascript">
									function changelogo() {
										//with (window.document.frmindex)
										{
											
											//alert (document.getElementById("srclogotn").src);
											//1	document.getElementById("srclogotn").src = "images/" + dbselect.value.split("|")[0] + "/logo.jpg";
											//alert (document.getElementById("srclogotn").src);
											//document.getElementById("clientlogo").src = "images/" + dbselect.value.split("|")[1];
										}
									}
								</script>
								<!-- <?php if ($selectedclientcode != "") { ?> <img id="srclogotn" src="images/<?php echo $clientcode; ?>/logo.jpg"><?php } else { ?> <img id="srclogotn" src=""> <?php } ?>
							<?php } // Customer 
							?> -->
						</div>
					</div>

					<div class="login__field">
						<i class="login__icon fas fa-user"></i>
						<input type="text" name="frmusername" class="login__input" placeholder="User ID / Email">
					</div>
					<div class="login__field">
						<i class="login__icon fas fa-lock"></i>
						<input type="password" name="frmpassword" class="login__input" placeholder="Password">
					</div>
					<button class="button login__submit">
						<span class="button__text">Sign In </span>
						<i class="button__icon fas fa-chevron-right"></i>
					</button>
				</form>
				<div class="social-login">
					<h7>An Energy Handbook</h3>
						<div class="social-icons">
							<img width="100" height="95" src="./images/spzlogo.jpg" class="social-login__icon">
							<img id="clientlogo" src="./images/sbdmlogo.jpg" class="social-login__icon">

						</div>
				</div>
			</div>
			<div class="screen__background">
				<span class="screen__background__shape screen__background__shape4"></span>
				<span class="screen__background__shape screen__background__shape3"></span>
				<span class="screen__background__shape screen__background__shape2"></span>
				<span class="screen__background__shape screen__background__shape1"></span>
			</div>
		</div>
	</div>
</body>

</html>