<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>BankMaster</title>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h4 id="head">Bank <small class="text-muted">Table</small></h4>
        </div>
        <div class="col-sm-6">
          <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add Details</button>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <!-- -----------------------------------FORM---------------------------------------- -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-info" id="form" hidden>
            <div class="card-header">
              <h3 class="card-title">Bank Form</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="card-body">
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-2 col-form-label">Bank Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="bankname" name="bankname" placeholder="Enter Bank Name">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-2 col-form-label">Branch name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="branchname" name="branchname" placeholder="Enter Branch Name">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-2 col-form-label">IFSC Code</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ifsc" name="ifsc" placeholder="Enter IFSC Code">
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class=row>
                  <div class="col-sm-3">
                    <button type="button" class="btn btn-success float-right" onclick="save()"><i class="fas fa-save">
                      </i>
                      Save</button>
                  </div>
                  <div class="col-sm-2">
                    <!-- <button type="submit" class="btn btn-default float-right">Cancel</button> -->
                    <button type="button" class="btn btn-danger float-left" id="cancel">
                      <i class="fas fa-times-circle"></i>
                      <span>Cancel</span>
                    </button>
                  </div>
                </div>
              </div>
              <!-- /.card-footer -->
            </form>
          </div>
          <!-- --------------DATATABLE----------------------------------------------- -->
          <div class="card card-info" id="dt">

            <div class="card-header">
              <h3 class="card-title">Bank Details</h3>
            </div>
            <div class="loaderclass" hidden>
            </div>
            <!-- /.card-header -->
            <div class="divclass">
              <div class="card-body" id="bank_dt">
              </div>
            </div>


            <!-- /.card-body -->
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<script>
  var bank_id = 0;
  $(document).ready(function() {
    $("#additem").click(function() {
      $("#form").show().removeAttr('hidden');
      $("#dt").hide();
      $("#additem").hide();
      $("#head").hide();
      clear();
      bank_id = 0;

    });
    $("#cancel").click(function() {
      $("#dt").show();
      $("#form").hide();
      $("#additem").show();
      $("#head").show();
      clear();

    });
    datatable();
  });
  // --------------------------------------DATA TABLE FUNCTION----------------
  function datatable() {
    //DATA TABLE CALLED
    loaderstart("divclass", "loaderclass");
    $.ajax({
      type: "POST",
      url: "bankmasterget.php",
      success: function(response) {
        $("#bank_dt").html(response);
        loaderend("divclass", "loaderclass");
      }
    })
    $("#form").hide();
    $("#dt").show();
    $("#additem").show();
    $("#head").show();
  }
  //---------------------------------------------SAVE FUNCTION------------------------
  function save() {
    //SAVE
    var type = "";
    if (bank_id == 0) {
      type = "save"
    } else {
      type = "update"
    }
    //validate
    var name = $('#name').val();
    var bankname = $('#bankname').val();
    var branchname = $('#branchname').val();
    var ifsc = $('#ifsc').val();
    if (name == "" || name == null) {
      statusswal("Name must be filled out", 'warning');
      return false;
    }
    if (bankname == "" || bankname == null) {
      statusswal("Bankname must be filled out", 'warning');
      return false;
    }
    if (branchname == "" || branchname == null) {
      statusswal("Branchname must be filled out", 'warning');
      return false;
    }
    if (ifsc == "" || ifsc == null) {
      statusswal("IFSC must be filled out", 'warning');
      return false;
    }
    $.ajax({
      type: "POST",
      data: {
        data: bank_id,
        type: type,
        name: name,
        bankname: bankname,
        branchname: branchname,
        ifsc: ifsc,
      },
      url: "../master/bankmasterset.php",
      success: function(response) {
        //Alert
        if (response == "0") {

          if (bank_id == 0) {
            statusswal("Bank Details failed to Create", 'error');
          } else {
            statusswal("Bank Details failed to Update", 'error');
          }

        } else {
          if (bank_id == 0) {
            statusswal("Bank Details created successfully", 'success');
          } else {
            statusswal("Bank Details Updated successfully", 'success');
          }

          if (response == "2") {
            statusswal("Employee name Already Exist", 'error');
          }
          datatable();
        }

      }
    })
  }
  //---------------------------------EDIT FUNCTION---------------------
  function editid(id) {
    //EDIT
    bank_id = id;
    var type = "edit";
    $("#dt").hide();
    $("#form").show().removeAttr('hidden');
    $.ajax({
      type: "POST",
      data: {
        "data": bank_id,
        "type": type
      },
      url: "../master/bankmasterset.php",
      success: function(response) {
        var json = $.parseJSON(response);
        $('#name').val(json[0].name);
        $('#bankname').val(json[0].bank_name);
        $('#branchname').val(json[0].branch_name);
        $('#ifsc').val(json[0].ifsc_code);
      }
    })
  }
  // function removeemployee(id) {
  //   //REMOVE
  //   bank_id = id;
  //   var type = "";
  //   if (bank_id != "" && bank_id != null) {
  //     type = "delete"
  //   }
  //   $.ajax({
  //     type: "POST",
  //     data: {
  //       "data": bank_id,
  //       "type": type
  //     },
  //     url: "../master/bankmasterset.php",
  //     success: function(dat) {
  //       alert(dat)
  //       datatable();
  //     }
  //   })
  // }
  //----------------------------------------CLEAR FUNCTION-----------
  function clear() {
    //CLEAR
    $('#name').val('');
    $('#bankname').val('');
    $('#branchname').val('');
    $('#ifsc').val('');
  }
</script>
<!-- /.content-wrapper -->
<?php include("../../include/footer.php"); ?>