<?php include("../../connectionsettings.inc"); ?>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Bank Name</th>
            <th>Branch Name</th>
            <th>IFSC Code</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $qry = "SELECT `bank_id`, `name`, `bank_name`, `branch_name`, `ifsc_code` FROM `bank_master`";
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);

        ?>
        <?php
        while ($row = mysqli_fetch_array($result)) {
        ?>
            <tr>
                <td><?php echo $row['name']; ?> </td>
                <td><?php echo $row['bank_name']; ?></td>
                <td><?php echo $row['branch_name']; ?></td>
                <td><?php echo $row['ifsc_code']; ?></td>
                <td> <span><a alt="Edit" href="javascript:editid(<?php echo $row['bank_id']; ?>)"><button class="btn btn-info btn-sm">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button></a></span>
                    <!-- <span> <a alt="Delete" href="javascript:removeemployee(<?php echo $row['bank_id']; ?>)"><button class="btn btn-danger btn-sm">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button></a></span> -->
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>