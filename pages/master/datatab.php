<table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Employee_ID</th>
                            <th>Id_Code</th>
                            <th>First_Name</th>
                            <th>Last_Name</th>
                            <th>Father_Name</th>
                            <th>Department_Name</th>
                            <th>Designation_Name</th>
                            <th>D.O.B</th>
                            <th>D.O.J</th>
                            <th>Edit</th>
                            <th>Delete</th>
                            <th>Account_Number</th>
                            
                            <th>Bank_Id</th>
                            <th>PF_Number</th>
                            <th>ESI_Number</th>
                            <th>PAN_Number</th>
                            <th>Adhaar_Number</th>
                            <th>Email_Id</th>
                            <th>Residentia_Address</th>
                            <th>Permanent_Address</th>
                            <th>Phone_Number</th>
                            <th>Relation_Name</th>
                            <th>Relationship</th>
                            <th>Relation_ContactNumber</th>
                            <th>Passport_Number</th>
                            <th>Active</th>
                            <th>Active_TillDate</th>
                            <th>Isadmin</th>
                            <th>Gross_Salary</th>
                            <th>Isuser</th>





                        </tr>
                    </thead>
                    <tbody>
                        <?php $qry = "SELECT `emp_id`, `id_code`, `first_name`, `last_name`, `father_name`, `department_id`, `designation_id`, `dob`, `doj`, `account_number`, `bank_id`, `pf_number`, `esi_number`, `pan_number`, `aadhar_number`, `email_id`, `address`, `permanent_address`, `phone_number`, `relation_name`, `relationship`, `relation_contactnumber`, `passport_number`, `active`, `active_tilldate`, `isadmin`, `gross_salary`, `isuser` FROM `employee`";
                        $conn = mysqli_connect("localhost", "root", "spz", "attendance");
                        $result = mysqli_query($conn, $qry);
                        $count = mysqli_num_rows($result);
                        if ($count == 0) {
                        ?>
                            <tr>
                                <th>No Details Found</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            <?php
                        } else {
                            while ($row = mysqli_fetch_array($result)) {
                            ?>
                                <tr>
                                    <td><?php echo $row['emp_id']; ?> </td>
                                    <td><?php echo $row['id_code']; ?> </td>
                                    <td><?php echo $row['first_name']; ?></td>
                                    <td><?php echo $row['last_name']; ?></td>
                                    <td><?php echo $row['father_name']; ?></td>
                                    <td><?php echo $row['department_id']; ?></td>
                                    <td><?php echo $row['designation_id']; ?> </td>
                                    <td><?php echo $row['dob']; ?> </td>
                                    <td><?php echo $row['doj']; ?></td>
                                    <td> <a alt="Edit" href="javascript:editid(<?php echo $row['emp_id']; ?>)"><button class="btn btn-info btn-sm">
                                                        <i class="fas fa-pencil-alt">
                                                        </i>
                                                        Edit
                                                    </button></a></td>
                                                    <td> <a alt="Delete" href="javascript:removeemployee(<?php echo $row['emp_id']; ?>)"><button class="btn btn-danger btn-sm">
                                                        <i class="fas fa-trash">
                                                        </i>
                                                        Delete
                                                    </button></a></td>
                                            <!-- <td><button class="btn btn-danger btn-sm" id="delete" value="<?php //echo $row['emp_id']; ?>">
                                                    <i class="">
                                                    </i>
                                                    Delete
                                                </button></td> -->
                                    <td><?php echo $row['account_number']; ?></td>
                                    
                                    <td><?php echo $row['bank_id']; ?></td>
                                    <td><?php echo $row['pf_number']; ?></td>
                                     <td><?php echo $row['esi_number']; ?> </td>
                                    <td><?php echo $row['pan_number']; ?> </td>
                                    <td><?php echo $row['aadhar_number']; ?></td>
                                    <td><?php echo $row['email_id']; ?></td>
                                    <td><?php echo $row['address']; ?></td>
                                    <td><?php echo $row['permanent_address']; ?></td>
                                    <td><?php echo $row['phone_number']; ?> </td>
                                    <td><?php echo $row['relation_name']; ?> </td>
                                    <td><?php echo $row['relationship']; ?></td>
                                    <td><?php echo $row['relation_contactnumber']; ?></td>
                                    <td><?php echo $row['passport_number']; ?></td>
                                    <td><?php echo $row['active']; ?></td>
                                    <td><?php echo $row['active_tilldate']; ?></td>
                                    <td><?php echo $row['isadmin']; ?></td>
                                    <td><?php echo $row['gross_salary']; ?></td>
                                    <td><?php echo $row['isuser']; ?></td>




                                 


                                </tr>
                            <?php } ?>
                        <?php
                        } ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Employee_ID</th>
                            <th>Id_Code</th>
                            <th>First_Name</th>
                            <th>Last_Name</th>
                            <th>Father_Name</th>
                            <th>Department_Name</th>
                            <th>Designation_Name</th>
                            <th>D.O.B</th>
                            <th>D.O.J</th>
                            <th>Edit</th>
                            <th>Delete</th>
                            <th>Account_Number</th>
                            <th>Bank_Id</th>
                            <th>PF_Number</th>
                            <th>ESI_Number</th>
                            <th>PAN_Number</th>
                            <th>Adhaar_Number</th>
                            <th>Email_Id</th>
                            <th>Residentia_Address</th>
                            <th>Permanent_Address</th>
                            <th>Phone_Number</th>
                            <th>Relation_Name</th>
                            <th>Relationship</th>
                            <th>Relation_ContactNumber</th>
                            <th>Passport_Number</th>
                            <th>Active</th>
                            <th>Active_TillDate</th>
                            <th>Isadmin</th>
                            <th>Gross_Salary</th>
                            <th>Isuser</th>
                        </tr>
                    </tfoot>
                </table>
                <script>
                         $(function() {
                $("#example1").DataTable({
                    "responsive": true,
                    "lengthChange": false,
                    "autoWidth": false,
                    "buttons": ["copy", "csv", "excel", "print", "colvis"]
                }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            });
                </script>