<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>DepartmentMaster</title>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h4 id="head">Department <small class="text-muted">Table</small></h4>
        </div>
        <div class="col-sm-6">
          <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add Details</button>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <!-- -----------------FORM----------------------------------------------- -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-info" id="form" hidden>
            <div class="card-header">
              <h3 class="card-title">Department Form</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="card-body">
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-2 col-form-label">Department Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="departmentname" name="departmentname" placeholder="Enter Department Name">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-2 col-form-label">Remarks</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="3" placeholder="Enter Remarks" name="remarks" id="remarks"></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-2 col-form-label">Priority</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="priority" name="priority" placeholder="Enter Priority">
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class=row>
                  <div class="col-sm-3">
                    <button type="button" class="btn btn-success float-right" onclick="save()"><i class="fas fa-save">
                      </i>
                      Save</button>
                  </div>
                  <div class="col-sm-2">
                    <!-- <button type="submit" class="btn btn-default float-right">Cancel</button> -->
                    <button type="button" class="btn btn-danger float-left" id="cancel">
                      <i class="fas fa-times-circle"></i>
                      <span>Cancel</span>
                    </button>
                  </div>
                </div>
              </div>
              <!-- /.card-footer -->
            </form>
          </div>
          <!-- ---------------------DATATABLE---------------------- -->
          <div class="card card-info" id="dt">
            <div class="card-header">
              <h3 class="card-title">Department Details</h3>
            </div>
            <div class="loaderclass" hidden>
            </div>
            <!-- /.card-header -->
            <div class="divclass">
              <div class="card-body" id="dep_dt">
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<script>
  var d_id = 0;
  $(document).ready(function() {

    $("#additem").click(function() {

      $("#form").show().removeAttr('hidden');
      $("#dt").hide();
      $("#additem").hide();
      $("#head").hide();
      clear();
      d_id = 0;

    });
    $("#cancel").click(function() {
      $("#dt").show();
      $("#form").hide();
      $("#additem").show();
      $("#head").show();
      clear();

    });
    datatable();
  });
  // --------------------------------------DATA TABLE FUNCTION----------------
  function datatable() {
    //DATATABLE CALLED
    loaderstart("divclass", "loaderclass");
    $.ajax({
      type: "POST",
      url: "departmentmasterget.php",
      success: function(response) {
        $("#dep_dt").html(response);
        loaderend("divclass", "loaderclass");
      }
    })
    $("#form").hide();
    $("#dt").show();
    $("#additem").show();
    $("#head").show();
  }
  //---------------------------------------------SAVE FUNCTION------------------------
  function save() {
    //SAVE
    var type = "";
    if (d_id == 0) {
      type = "save"
    } else {
      type = "update"
    }
    //validate
    var departmentname = $('#departmentname').val();
    var remarks = $('#remarks').val();
    var priority = $('#priority').val();
    if (departmentname == "" || departmentname == null) {
      statusswal("Department Name must be filled out", 'warning');
      return false;
    }
    if (remarks == "" || remarks == null) {
      statusswal("Remarks Name must be filled out", 'warning');
      return false;
    }
    if (priority == "" || priority == null) {
      statusswal("Priority Name must be filled out", 'warning');
      return false;
    }
    $.ajax({
      type: "POST",
      data: {
        data: d_id,
        type: type,
        departmentname: departmentname,
        remarks: remarks,
        priority: priority,
      },
      url: "../master/departmentmasterset.php",
      success: function(response) {
        //Alert
        if (response == "0") {
          if (d_id == 0) {
            statusswal("Department failed to Create", 'error');
          } else {
            statusswal("Department failed to Update", 'error');
          }

        } else {
          if (d_id == 0) {
            statusswal("Department created successfully", 'success');
          } else {
            statusswal("Department Updated successfully", 'success');
          }
          if (response == "2") {
            statusswal("Department name Already Exist", 'error');
          }
          datatable();
        }


      }
    })
  }
  //---------------------------------EDIT FUNCTION---------------------
  function editid(id) {
    //EDIT
    d_id = id;
    var type = "edit";
    $("#dt").hide();
    $("#additem").hide();
    $("#head").hide();
    $("#form").show().removeAttr('hidden');
    $.ajax({
      type: "POST",
      data: {
        "data": d_id,
        "type": type
      },
      url: "../master/departmentmasterset.php",
      success: function(response) {
        var json = $.parseJSON(response);
        // console.log(json)

        $('#departmentname').val(json[0].department_name);
        $('#remarks').val(json[0].remarks);
        $('#priority').val(json[0].priority);
      }
    })
  }
  // function removeemployee(id) {
  //   //REMOVE
  //   department_id = id;
  //   var type = "";
  //   if (department_id != "" && department_id != null) {
  //     type = "delete"
  //   }
  //   $.ajax({
  //     type: "POST",
  //     data: {
  //       "data": department_id,
  //       "type": type
  //     },
  //     url: "../master/departmentmasterset.php",
  //     success: function(dat) {
  //       alert(dat)
  //       datatable();
  //     }
  //   })
  // }
  //--------------------------CLEAR FUNCTION-------------------------
  function clear() {
    //CLEAR
    $('#departmentname').val('');
    $('#remarks').val('');
    $('#priority').val('');
  }
</script>
<!-- /.content-wrapper -->
<?php include("../../include/footer.php"); ?>