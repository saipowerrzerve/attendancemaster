<?php include("../../connectionsettings.inc"); ?>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Department Name</th>
            <th>Remarks</th>
            <th>Priority</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $qry = "SELECT `department_id`, `department_name`, `remarks`, `priority` FROM `departmentmaster`";
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {
        ?>
            <tr>
                <td><?php echo $row['department_name']; ?> </td>
                <td><?php echo $row['remarks']; ?></td>
                <td><?php echo $row['priority']; ?></td>
                <td> <span><a alt="Edit" href="javascript:editid(<?php echo $row['department_id']; ?>)"><button type="button" class="btn btn-info btn-sm">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button></a></span>
                    <!-- <span><a alt="Delete" href="javascript:removeemployee(<?php echo $row['department_id']; ?>)"><button type="button" class="btn btn-danger btn-sm">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button></a></span> -->
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>