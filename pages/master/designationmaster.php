<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>Desigination Master</title>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid " id="add">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Designation Master</h1>
                </div>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add item</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="form" hidden>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Designation Form</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="desg_name" class="col-sm-2 col-form-label">Designation_name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="designation_name" placeholder="Designation_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="remarks" class="col-sm-2 col-form-label">Remarks</label>
                                    <div class="col-sm-10">
                                        <textarea id="remarks" class="form-control" rows="3" placeholder="Remarks ..."></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Priority" class="col-sm-2 col-form-label">Priority</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="priority" placeholder="Priority">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <button type="button" class="btn  btn-success float-right" onclick="save()">
                                            <i class="fas fa-save"></i> Save</button>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="button" id="cancel" class="btn  btn-danger float-left">
                                            <i class="fas fa-times-circle"></i> Cancel</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
        <div class="container-fluid" id="table">
            <div class="loaderclass" hidden>
            </div>
            <div class="divclass">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Designation Details</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" id="emp_dt">
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    var designation_id = 0;
    $(document).ready(function() {

        $("#additem").click(function() {
            $("#table").hide();
            $("#form").show().removeAttr('hidden');
            $("#add").hide();
            clear();
            designation_id = 0;
        });
        $("#cancel").click(function() {
            $("#table").show();
            $("#form").hide();
            $("#add").show();
        });
        datatable();
    });
    // -------------------------------------table---------------------------
    function datatable() {
        $("#add").show();
        $("#form").hide();
        $("#table").show();
        loaderstart("divclass", "loaderclass");
        $.ajax({
            type: "GET",
            url: "designationmasterget.php",
            success: function(response) {
                $("#emp_dt").html(response);
                loaderend("divclass", "loaderclass");
            }
        })
    }
    // -------------------------------------end---------------------------
    function save() {

        var designation_name = $("#designation_name").val();
        var remarks = $("#remarks").val();
        var priority = $("#priority").val();
        if (designation_name == "" || designation_name == null) {
            Swal.fire("", "designation name must be filled out", 'warning')
            return false;
        }
        if (remarks == "" || remarks == null) {
            Swal.fire("", "remarks must be filled out", 'warning')
            return false;
        }
        if (priority == "" || priority == null) {
            Swal.fire("", "Priority must be filled out", 'warning')
            return false;
        }
        var type = "";
        if (designation_id == 0) {
            type = "save"
        } else {
            type = "update"
        }
        $.ajax({
            type: "POST",
            data: {
                data: designation_id,
                type: type,
                designation_name: designation_name,
                remarks: remarks,
                priority: priority,
            },
            url: "designationmasterset.php",
            success: function(response) {
                if (response == "0") {
                    statusswal('desigination created failed', 'error');
                } else {
                    statusswal('desigination created successfully', 'success');
                    datatable();
                }
            }
        })
    }
    // ------------------------------------edit-------------------------------
    function editid(id, designation_name, remarks, priority) {
        designation_id = id;
        // alert(designation_id);
        $("#designation_name").val(designation_name);
        $("#remarks").val(remarks);
        $("#priority").val(priority);
        $("#table").hide();
        $("#form").show().removeAttr('hidden');
        $("#add").hide();
    }
    // -------------------------delete----------------------------------------
    // function removeemployee(id) {

    //     designation_id = id;
    //     var type = "";
    //     if (designation_id != "" && designation_id != null) {
    //         type = "delete"
    //     }
    //     Swal.fire({
    //         title: 'Are you sure?',
    //         text: "You won't be able to revert this!",
    //         icon: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, delete it!'
    //     }).then((result) => {
    //         if (result.isConfirmed) {
    //             $.ajax({
    //                 type: "POST",
    //                 data: {
    //                     "data": designation_id,
    //                     "type": type
    //                 },
    //                 url: "designationmasterset.php",
    //                 success: function(response) {
    //                     if (response == "0") {
    //                         Swal.fire("", 'desigination  removed. failed', 'error')
    //                     } else {
    //                         Swal.fire("", 'desigination  has been removed.', 'success')
    //                         datatable();
    //                     }
    //                 }
    //             })
    //         }
    //     })
    // }
    // -------------------------end----------------------------------------
    function clear() {
        $("#designation_name").val("");
        $("#remarks").val("");
        $("#priority").val("");
    }
</script>
<?php include("../../include/footer.php"); ?>