<?php include("../../connectionsettings.inc"); ?>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Designation</th>
            <th>Remarks</th>
            <th>Priority</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $qry = "SELECT `designation_id`, `designation_name`, `remarks`, `priority` 
        FROM `designationmaster` order by designation_id";
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        if ($count == 0) {
        ?>
            <?php
        } else {
            while ($row = mysqli_fetch_array($result)) {
            ?>
                <tr>
                    <td><?php echo $row['designation_name']; ?> </td>
                    <td><?php echo $row['remarks']; ?></td>
                    <td><?php echo $row['priority']; ?></td>
                    <td>
                    <span>
                    <button class="btn btn-info btn-sm" 
                    onclick="editid('<?php echo $row['designation_id']; ?>',
                    '<?php echo $row['designation_name']; ?>',
                    '<?php echo $row['remarks']; ?>','<?php echo $row['priority']; ?>')" >
                             <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button>
                             </span>  
                             <!-- <span>
                             <a alt="Delete" href="javascript:removeemployee(<?php echo $row['designation_id']; ?>)"><button class="btn btn-danger btn-sm">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button></a>
                             </span> -->
                  </td>
                </tr>
            <?php } ?>
        <?php
        } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>