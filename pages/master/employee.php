<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>Employee</title>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4 id="head">Employee
                        <!-- <small class="text-muted">Table</small> -->
                    </h4>
                </div>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add Details</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <!-- ------------------------------------FORM-------------------------------------------- -->
        <div class="container-fluid" id="form" hidden>
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Employee Form</h3>
                </div>
                <div class="card-body">
                    <form name="myForm" method="post" action="" enctype="multipart/form-data" id="myform">
                        <div class="row">
                            <div class="form-group">
                                <label><u>EMPLOYEE PERSONAL DETAILS</u></label>
                            </div>
                        </div>
                        <!-- <div class="card-body"> -->
                        <div class="row" id="actions">
                            <div class="col-lg-6">
                                <div class="btn-group w-100">
                                    <span class="btn btn-success col fileinput-button">
                                        <i class="fas fa-plus"></i>
                                        <span>Add files</span>
                                    </span>
                                    <button type="button" hidden class="btn btn-primary col start">
                                        <i class="fas fa-upload"></i>
                                        <span>Start upload</span>
                                    </button>
                                    <button type="button" class="btn btn-warning col cancel">
                                        <i class="fas fa-times-circle"></i>
                                        <span>Cancel upload</span>
                                    </button>
                                </div>
                            </div>
                            <!-- <div class="col-lg-6 d-flex align-items-center">
                                    <div class="fileupload-process w-100">
                                        <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                        </div>
                                    </div>
                                </div> -->
                        </div>
                        <div class="table table-striped files" id="previews">
                            <div id="template" class="row mt-2">
                                <div class="col-auto">
                                    <span class="preview"><img src="data:," alt="" data-dz-thumbnail /></span>
                                </div>
                                <div class="col d-flex align-items-center">
                                    <p class="mb-0">
                                        <span class="lead" data-dz-name></span>
                                        (<span data-dz-size></span>)
                                    </p>
                                    <strong class="error text-danger" data-dz-errormessage></strong>
                                </div>
                                <!-- <div class="col-4 d-flex align-items-center">
                                        <div class="progress progress-striped active w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                        </div>
                                    </div> -->
                                <div class="col-auto d-flex align-items-center">
                                    <div class="btn-group start">
                                        <!-- <button data-dz-remove class="btn btn-warning cancel">
                                                <i class="fas fa-times-circle"></i>
                                                <span>Cancel</span>
                                            </button>
                                            <button data-dz-remove class="btn btn-danger delete">
                                                <i class="fas fa-trash"></i>
                                                <span>Delete</span>
                                            </button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- </div> -->
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Employee Code</label>
                                    <input type="text" class="form-control" id="idcode" placeholder="Enter ..." name="idcode">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" placeholder="Enter First Name" name="firstname" id="firstname">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" placeholder="Enter Last Name" name="lastname" id="lastname">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Department Name</label>
                                    <select class="form-control select2bs4" name="departmentname" id="departmentname">
                                        <option value="" selected>Enter department name</option>
                                        <?php
                                        $sql = "SELECT department_id,`department_name` FROM `departmentmaster` ";
                                        $result = mysqli_query($dbh, $sql);
                                        while ($row = mysqli_fetch_array($result)) {
                                        ?>
                                            <option value="<?php echo $row['department_id']; ?>"><?php echo $row['department_name']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Designation Name</label>
                                    <select class="form-control select2bs4" name="designationname" id="designationname">

                                        <option value="" selected>Enter designation name</option>
                                        <?php
                                        $sql = "SELECT designation_id,`designation_name` FROM `designationmaster`";
                                        $result = mysqli_query($dbh, $sql);
                                        while ($row = mysqli_fetch_array($result)) {
                                        ?>
                                            <option value="<?php echo $row['designation_id']; ?>"><?php echo $row['designation_name']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Father Name</label>
                                    <input type="text" class="form-control" placeholder="Enter Father Name" name="fathername" id="fathername">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Of Birth</label>
                                    <div class="input-group date datedmy" id="dobdate" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#dobdate" placeholder="Enter DOB" name="dob" id="dobdate" />
                                        <div class="input-group-append" data-target="#dobdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Of Joining</label>
                                    <div class="input-group date datedmy" id="dojdate" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#dojdate" placeholder="Enter DOJ" name="doj" id="dojdate" />
                                        <div class="input-group-append" data-target="#dojdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Email Id</label>
                                    <input type="email" class="form-control" placeholder="Enter Email Id" name="emailid" id="emailid">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- textarea -->
                                <div class="form-group">
                                    <label>Residential Address</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter Residential Address" name="residentialaddress" id="residentialaddress"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Permanent Address</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter Permanent Address" name="permanentaddress" id="permanentaddress"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="number" class="form-control" placeholder="Enter Phone Number" name="phonenumber" id="phonenumber">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Relation Name</label>
                                    <input type="text" class="form-control" placeholder="Enter Relation Name" name="relationname" id="relationname">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>RelationShip</label>
                                    <input type="text" class="form-control" placeholder="Enter RelationShip" name="relationship" id="relationship">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Emergency Contact Number</label>
                                    <input type="number" class="form-control" placeholder="Enter Emergency Contact Number" name="emergencycontactnumber" id="emergencycontactnumber">
                                </div>
                            </div>
                        </div>
                        <div class="row" hidden>
                            <div class="form-group">
                                <label><u>EMPLOYEE BANK DETAILS</u></label>
                            </div>
                        </div>
                        <div class="row" hidden>
                            <div class="col-sm-3">

                                <div class="form-group">
                                    <label>Account Number</label>
                                    <input type="text" class="form-control" placeholder="Enter Account Number" name="accountnumber" id="accountnumber">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Bank Id</label>
                                    <input type="text" class="form-control" placeholder="Enter Bank Id" name="bankid" id="bankid">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Pan Number</label>
                                    <input type="text" class="form-control" placeholder="Enter Pan Number" name="pannumber" id="pannumber">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Adhar Number</label>
                                    <input type="text" class="form-control" placeholder="Enter Adhar Number" name="adharnumber" id="adharnumber">
                                </div>
                            </div>
                        </div>
                        <div class="row" hidden>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Pf Number</label>
                                    <input type="text" class="form-control" placeholder="Enter Pf Number" name="pfnumber" id="pfnumber">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>ESI Number</label>
                                    <input type="text" class="form-control" placeholder="Enter ESI Number" name="esinumber" id="esinumber">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Passport Number</label>
                                    <input type="text" class="form-control" placeholder="Enter Passport Number" name="passportnumber" id="passportnumber">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Gross Salary</label>
                                    <input type="number" class="form-control" placeholder="Enter Gross Salary" name="grosssalary" id="grosssalary">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="checkboxPrimary3" name="isuser" id="isuser">
                                <label for="checkboxPrimary3">
                                    IsUser
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <!-- <button type="submit" class="btn btn-block btn-success" id="save" name="submit" onclick="save()"><i class="fas fa-save"></i> SAVE</button> -->
                                <button type="button" class="btn  btn-success float-right" onclick="save()">
                                    <i class="fas fa-save">
                                    </i>
                                    Save
                                </button>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-danger float-left cancel" id="cancel">
                                    <i class="fas fa-times-circle"></i>
                                    <span>Cancel</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-------------- datatable-------------------------------------------------- -->
        <div class="card card-info" id="dt">
            <div class="card-header">
                <h3 class="card-title">Employee Details</h3>
            </div>
            <div class="loaderclass" hidden>
            </div>
            <div class="divclass">
                <div class="card-body" id="emp_dt">
                </div>
            </div>
            <!-- /.card-header -->
        </div>
    </section>
    <!-- </div> -->
</div>
<!-- /.content-wrapper -->
<script>
    var id_code = 0;
    var arr = "";
    $(document).ready(function() {
        $("#additem").click(function() {
            $("#idcode").prop('disabled', false);
            $("#form").show().removeAttr('hidden');
            $("#dt").hide();
            $("#additem").hide();
            $("#head").hide();
            clear();
            id_code = 0;
            myDropzone.removeAllFiles(true);
        });
        $("#cancel").click(function() {
            $("#dt").show();
            $("#form").hide();
            $("#additem").show();
            $("#head").show();
            clear();
        });
        datatable();
    });
    //---------------------------------------------datatable--------------------------------------//
    function datatable() {
       
        loaderstart("divclass", "loaderclass");
        //DATA TABLE CALLED
        $.ajax({
            type: "POST",
            url: "employeeget.php",
            success: function(response) {
                $("#emp_dt").html(response);
                loaderend("divclass", "loaderclass");
            }
        })
        $("#form").hide();
        $("#dt").show();
        $("#additem").show();
        $("#head").show();
    }
    //---------------------------------------------save------------------------------------//
    function save() {

        //SAVE
        var idcode = $('#idcode').val();
        var firstname = $('#firstname').val();
        var lastname = $('#lastname').val();
        var departmentname = $('#departmentname').val();
        var designationname = $('#designationname').val();
        var permanentaddress = $('#permanentaddress').val();
        var phonenumber = $('#phonenumber').val();

        if (idcode == "" || idcode == null) {
            Swal.fire("", "Idcode must be filled out", 'warning')
            return false;
        }
        if (firstname == "" || firstname == null) {
            Swal.fire("", "Firstname must be filled out", 'warning')
            return false;
        }
        if (lastname == "" || lastname == null) {
            Swal.fire("", "Lastname must be filled out", 'warning')
            return false;
        }
        if (departmentname == "" || departmentname == null) {
            Swal.fire("", "Departmentname must be filled out", 'warning')
            return false;
        }
        if (designationname == "" || designationname == null) {
            Swal.fire("", "Designationname must be filled out", 'warning')
            return false;
        }
        if (permanentaddress == "" || permanentaddress == null) {
            Swal.fire("", "Permanentaddress must be filled out", 'warning')
            return false;
        }
        if (phonenumber == "" || phonenumber == null) {
            Swal.fire("", "Phonenumber must be filled out", 'warning')
            return false;
        }
        if ($('#dobdate').data("datetimepicker").date() == "" || $('#dobdate').data("datetimepicker").date() == null) {
            Swal.fire("", "dobdate must be filled out", 'warning')
            return false;
        }
        if ($('#dojdate').data("datetimepicker").date() == "" || $('#dojdate').data("datetimepicker").date() == null) {
            Swal.fire("", "dojdate must be filled out", 'warning')
            return false;
        }

        var dobdate = formatDate($('#dobdate').data("datetimepicker").date());
        var dojdate = formatDate($('#dojdate').data("datetimepicker").date());

        function formatDate(date) {
            var d = new Date(date),
                month = "" + (d.getMonth() + 1),
                day = "" + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = "0" + month;
            if (day.length < 2) day = "0" + day;
            return [year, month, day].join("-");
        }
        var type = "";
        if (id_code == 0) {
            type = "save"
        } else {
            type = "update"
        }

        $.ajax({
                type: "POST",
                data: {
                    data: id_code,
                    type: type,
                    idcode: $('#idcode').val(),
                    firstname: $('#firstname').val(),
                    lastname: $('#lastname').val(),
                    departmentname: $('#departmentname').val(),
                    designationname: $('#designationname').val(),
                    fathername: $('#fathername').val(),
                    dob: dobdate,
                    doj: dojdate,
                    emailid: $('#emailid').val(),
                    residentialaddress: $('#permanentaddress').val(),
                    permanentaddress: $('#permanentaddress').val(),
                    phonenumber: $('#phonenumber').val(),
                    relationname: $('#relationname').val(),
                    relationship: $('#relationship').val(),
                    emergencycontactnumber: $('#emergencycontactnumber').val(),
                    isuser: $('#checkboxPrimary3').prop('checked'),
                },
                url: "../master/employeeset.php",
                success: function(response) {
                    // statusswal(response, 'error')
                    arr = response.split(',');

                    if (arr[0] == 0) {
                        statusswal('Employee failed to Create', 'error')
                    } else if (arr[0] == 1) {
                        statusswal('Employee created successfully', 'success')
                        datatable();
                    } else if (arr[0] == 2) {
                        statusswal('Employee failed to Update', 'error')
                    } else if (arr[0] == 3) {
                        statusswal('Employee Updated successfully', 'success')
                         datatable();
                    } else if (arr[0] == 4) {
                        statusswal('Employee code already taken', 'warning')
                    }
                }
            })
            .done(function(response) {
                if (arr[0] == 1 || arr[0] == 3) {
                    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
                    myDropzone.on("sending", function(file, xhr, formData) {
                        formData.append("id_code", arr[1]);
                        // alert(arr[1])
                    });
                    myDropzone.on("success", function(file, responseText) {
                        /// console.log(responseText)
                          datatable();
                        // window.location.reload();
                        
                    });
                }
            });
    }
    //---------------------------------------------edit------------------------------------//
    function editid(id) {
        //EDIT
       // $("#tstimage").attr('src','');
        myDropzone.removeAllFiles(true);
        id_code = id;
        var type = "edit";
        $("#idcode").prop('disabled', true);
        $("#dt").hide();
        $("#additem").hide();
        $("#form").show().removeAttr('hidden');
        var thumb;
        $.ajax({
            type: "POST",
            data: {
                "data": id_code,
                "type": type
            },
            url: "../master/employeedetails.php",
            success: function(response) {
                var json = $.parseJSON(response);
                thumb = {
                    name: json[0].id_code + '.jpg',
                    size: 1234,
                    dataURL: 'emp_images/' + json[0].id_code + '.jpg'
                };
                if (json[0].isuser == '1') {
                    $('#checkboxPrimary3').attr('checked', 'checked');
                } else {
                    $('#checkboxPrimary3').removeAttr('checked');
                }
                $('#idcode').val(json[0].id_code);
                $('#firstname').val(json[0].first_name)
                $('#lastname').val(json[0].last_name)
                $('#departmentname').val(json[0].department_id).trigger('change');
                $('#designationname').val(json[0].designation_id).trigger('change');
                $('#fathername').val(json[0].father_name)
                $('#dobdate').datetimepicker();
                $('#dobdate').datetimepicker('date', moment(json[0].dob, 'YYYY/MM/DD'));
                // -----------------------------------------------------
                $('#dojdate').datetimepicker();
                $('#dojdate').datetimepicker('date', moment(json[0].doj, 'YYYY/MM/DD'));
                $('#emailid').val(json[0].email_id)
                $('#residentialaddress').val(json[0].address)
                $('#permanentaddress').val(json[0].permanent_address)
                $('#phonenumber').val(json[0].phone_number)
                $('#relationname').val(json[0].relation_name)
                $('#relationship').val(json[0].relationship)
                $('#emergencycontactnumber').val(json[0].relation_contactnumber)

                myDropzone.files.push(thumb);
                // Call the default addedfile event handler
                myDropzone.emit('addedfile', thumb);

                myDropzone.createThumbnailFromUrl(thumb,
                    myDropzone.options.thumbnailWidth, myDropzone.options.thumbnailHeight,
                    myDropzone.options.thumbnailMethod, true,
                    function(thumbnail) {
                        myDropzone.emit('thumbnail', thumb, thumbnail);
                    });
                // Make sure that there is no progress bar, etc...
                myDropzone.emit('complete', thumb);
            }
        })
        // var fileurl = "emp_images/spz0001.jpg";
    }

    // function removeemployee(id) {
    //     //REMOVE
    //     id_code = id;
    //     var type = "";
    //     if (id_code != "" && id_code != null) {
    //         type = "delete"
    //     }
    //     $.ajax({
    //         type: "POST",
    //         data: {
    //             "data": id_code,
    //             "type": type
    //         },
    //         url: "../master/employeeset.php",
    //         success: function(dat) {
    //             alert(dat)
    //             datatable();
    //         }
    //     })
    // }
    //---------------------------------------------clear------------------------------------//
    function clear() {
        //CLEAR
        $('#idcode').val('');
        $('#firstname').val('');
        $('#lastname').val('');
        $('#departmentname').val('').trigger('change');
        $('#designationname').val('').trigger('change');
        $('#fathername').val('');
        $('#dobdate').data("datetimepicker").date(null)
        $('#dojdate').data("datetimepicker").date(null)
        $('#emailid').val('');
        $('#residentialaddress').val('');
        $('#permanentaddress').val('');
        $('#phonenumber').val('');
        $('#relationname').val('');
        $('#relationship').val('');
        $('#emergencycontactnumber').val('');
        $('#accountnumber').val('');
        $('#bankid').val('');
        $('#pannumber').val('');
        $('#adharnumber').val('');
        $('#pfnumber').val('');
        $('#esinumber').val('');
        $('#passportnumber').val('');
        $('#grosssalary').val('');
    }
</script>
<?php include("../../include/footer.php"); ?>