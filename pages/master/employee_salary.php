<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>Salary</title>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid " id="add">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Employee Salary</h1>
                </div>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add Details</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="form" hidden>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Salary Form</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form">
                            <div class="card-body">
                                <div class="row" >
                                    <div class="col-sm-3" id="emp_row">
                                        <label for="emp_drp" class="col-form-label">Employee</label>
                                        <select class="form-control form-control-md select2bs4" id="emp_drp" style="height:100%;width: 100%;">
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="department_id" class="col-form-label">Department</label>
                                        <select class="form-control form-control-md select2bs4" id="department_id" style="height:100%;width: 100%;">
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="designation_id" class="col-form-label">Designation</label>
                                        <select class="form-control form-control-md select2bs4" id="designation_id" style="height:100%;width: 100%;">
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="shift_id" class="col-form-label">Shift</label>
                                        <select class="form-control form-control-md select2bs4" id="shift_id" style="height:100%;width: 100%;">
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="bank_id" class="col-form-label">Bank</label>
                                        <select class="form-control form-control-md select2bs4" id="bank_id" style="height:100%;width: 100%;">
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="grosssalary" class="col-form-label">Gross salary</label>
                                        <input type="text" class="form-control form-control-md salary" id="grosssalary" placeholder="grosssalary">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="basic" class="col-form-label">Basic</label>
                                        <input type="text" class="form-control form-control-md salary" id="basic" placeholder="basic">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="da" class="col-form-label">da</label>
                                        <input type="text" class="form-control form-control-md salary" id="da" placeholder="da">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="hra" class="col-form-label">hra</label>
                                        <input type="text" class="form-control form-control-md salary" id="hra" placeholder="hra">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="conveyance" class="col-form-label">Conveyance</label>
                                        <input type="text" class="form-control form-control-md salary" id="conveyance" placeholder="conveyance">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="medicalallowance" class="col-form-label">Medicalallowance</label>
                                        <input type="text" class="form-control form-control-md salary" id="medicalallowance" placeholder="medicalallowance">
                                    </div>
                                    <div class="col-sm-3 ">
                                        <label for="specialallowance" class="col-form-label">Specialallowance</label>
                                        <input type="text" class="form-control form-control-md salary" id="specialallowance" placeholder="specialallowance">
                                    </div>
                                    <div class="col-sm-3 ">
                                        <label for="otherallowances" class="col-form-label">Otherallowances</label>
                                        <input type="text" class="form-control form-control-md salary" id="otherallowances" placeholder="otherallowances">
                                    </div>
                                    <div class="col-sm-3 ">
                                        <label for="account_number" class="col-form-label">Account number</label>
                                        <input type="text" class="form-control form-control-md  " id="account_number" placeholder="account_number">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col">
                                        <button type="button" class="btn  btn-success float-right" onclick="save()">
                                            <i class="fas fa-save"></i> Save</button>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" id="cancel" class="btn  btn-danger float-left">
                                            <i class="fas fa-times-circle"></i> Cancel</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
        <div class="container-fluid" id="table">
            <div class="loaderclass" hidden>
            </div>
            <div class="divclass">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Salary Details</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" id="usr_dt">
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    var emp_salary_id = 0;
    $(document).ready(function() {
        $("#emp_row").removeAttr('hidden');
        $("#additem").click(function() {
            $("#table").hide();
            $("#emp_row").removeAttr('hidden');
            $("#form").show().removeAttr('hidden');
            $("#add").hide();
            clear();
            emp_salary_id = 0;
            LoadComboboxFast("emp_drp");
            LoadComboboxFast('department_id');
            LoadComboboxFast('designation_id');
            LoadComboboxFast('shift_id');
            LoadComboboxFast('bank_id');

        });
        $("#cancel").click(function() {
            $("#table").show();
            $("#form").hide();
            $("#add").show();
        });
        datatable();
        // LoadComboboxFast();
       
    });
    // -------------------------------------table---------------------------
    function datatable() {
        LoadComboboxFast("emp_drp");
        LoadComboboxFast('department_id');
        LoadComboboxFast('designation_id');
        LoadComboboxFast('shift_id');
        LoadComboboxFast('bank_id');
        
        $("#add").show();
        $("#form").hide();
        $("#table").show();
        loaderstart("divclass", "loaderclass");
        $.ajax({
            type: "GET",
            url: "employee_salary_get.php",
            success: function(response) {
                $("#usr_dt").html(response);
                loaderend("divclass", "loaderclass");
            }
        })
    }
    // -------------------------------------Dropdown---------------------------
    function LoadComboboxFast(drp_typ) {
        $("#" + drp_typ).empty();
        $.ajax({
            type: "GET",
            data: {
                data: drp_typ,
            },
            url: "employee_salary_details.php",
            success: function(response) {}
        }).done(function(result) {
            $("#" + drp_typ).html(result);
        });
        $("#department_id").val(null).trigger('change');
        $("#designation_id").val(null).trigger('change');
        $("#shift_id").val(null).trigger('change');
        $("#bank_id").val(null).trigger('change');
    }
    //----------------------------------------save------------------------------------------------
    function save() {
        var emp_drp = $("#emp_drp").val();
        var department_id = $("#department_id").val();
        var designation_id = $("#designation_id").val();
        var shift_id = $("#shift_id").val();
        var bank_id = $("#bank_id").val();
        var grosssalary = $("#grosssalary").val().replace(/()_/g, '');
        var basic = $("#basic").val().replace(/()_/g, '');
        var da = $("#da").val().replace(/()_/g, '');
        var hra = $("#hra").val().replace(/()_/g, '');
        var conveyance = $("#conveyance").val().replace(/()_/g, '');
        var medicalallowance = $("#medicalallowance").val().replace(/()_/g, '');
        var specialallowance = $("#specialallowance").val().replace(/()_/g, '');
        var otherallowances = $("#otherallowances").val().replace(/()_/g, '');
        var account_number = $("#account_number").val();

        if (emp_salary_id == 0) {
            if (emp_drp == "" || emp_drp == null) {
                Swal.fire("", "employee name must be filled out", 'warning')
                return false;
            }
        }
        if (department_id == "" || department_id == null) {
            Swal.fire("", "department must be filled out", 'warning')
            return false;
        }
        if (designation_id == "" || designation_id == null) {
            Swal.fire("", "designation must be filled out", 'warning')
            return false;
        }
        if (shift_id == "" || shift_id == null) {
            Swal.fire("", "shift must be filled out", 'warning')
            return false;
        }
        if (bank_id == "" || bank_id == null) {
            Swal.fire("", "bank must be filled out", 'warning')
            return false;
        }
        if (grosssalary == "" || grosssalary == null) {
            Swal.fire("", "grosssalary must be filled out", 'warning')
            return false;
        }
        if (basic == "" || basic == null) {
            Swal.fire("", "basic must be filled out", 'warning')
            return false;
        }
        if (da == "" || da == null) {
            Swal.fire("", "da must be filled out", 'warning')
            return false;
        }
        if (hra == "" || hra == null) {
            Swal.fire("", "hra must be filled out", 'warning')
            return false;
        }
        if (conveyance == "" || conveyance == null) {
            Swal.fire("", "conveyance must be filled out", 'warning')
            return false;
        }
        if (medicalallowance == "" || medicalallowance == null) {
            Swal.fire("", "medicalallowance must be filled out", 'warning')
            return false;
        }
        if (specialallowance == "" || specialallowance == null) {
            Swal.fire("", "specialallowance must be filled out", 'warning')
            return false;
        }
        if (otherallowances == "" || otherallowances == null) {
            Swal.fire("", "otherallowances must be filled out", 'warning')
            return false;
        }
        if (account_number == "" || account_number == null) {
            Swal.fire("", "account_number must be filled out", 'warning')
            return false;
        }
        var type = "";
        if (emp_salary_id == 0) {
            type = "save"
        } else {
            type = "update"
        }
        //console.log(type, designation_id, designation_name, remarks, priority);
        $.ajax({
            type: "POST",
            data: {
                data: emp_salary_id,
                type: type,
                emp_drp_dt: emp_drp,
                department_id_dt: department_id,
                designation_id_dt: designation_id,
                shift_id_dt: shift_id,
                bank_id_dt: bank_id,
                grosssalary_dt: grosssalary,
                basic_dt: basic,
                da_dt: da,
                hra_dt: hra,
                conveyance_dt: conveyance,
                medicalallowance_dt: medicalallowance,
                specialallowance_dt: specialallowance,
                otherallowances_dt: otherallowances,
                account_number_dt: account_number
            },
            url: "employee_salary_set.php",
            success: function(response) {
                if (response == "0") {
                    statusswal('Salary created failed ', 'error');
                } else if (response == "1") {
                    statusswal('Salary created successfully', 'success');
                    datatable();
                }
            }
        })
    }
    //--------------------------------------EDIT------------------------------------//
    function editid(id, grosssalary, basic, da,hra,conveyance,medicalallowance,specialallowance,otherallowances,department_id,designation_id,shift_id,bank_id,account_number) {
        emp_salary_id = id;
        $("#emp_row").attr('hidden', 'hidden');
        $("#grosssalary").val(grosssalary);
        $("#basic").val(basic);
        $("#da").val(da);
        $("#hra").val(hra);
        $("#conveyance").val(conveyance);
        $("#basic").val(basic);
        $("#medicalallowance").val(medicalallowance);
        $("#specialallowance").val(specialallowance);
        $("#otherallowances").val(otherallowances);
        $("#department_id").val(department_id).trigger('change');
        $("#designation_id").val(designation_id).trigger('change');
        $("#shift_id").val(shift_id).trigger('change');
        $("#bank_id").val(bank_id).trigger('change');
        $("#account_number").val(account_number);

        $("#table").hide();
        $("#form").show().removeAttr('hidden');
        $("#add").hide();
    }
   //-------------------------------------CLEAR--------------------------------------//
    function clear() {
        $("#grosssalary").val(0);
        $("#basic").val(0);
        $("#da").val(0);
        $("#hra").val(0);
        $("#conveyance").val(0);
        $("#basic").val(0);
        $("#medicalallowance").val(0);
        $("#specialallowance").val(0);
        $("#otherallowances").val(0);
        $("#account_number").val('');
    }
</script>
<?php include("../../include/footer.php"); ?>