<?php include("../../connectionsettings.inc"); ?>

<?php
$type = $_GET['data'];
if ($type == "emp_drp") {
    $sqlquery = "SELECT id_code,emp_id FROM employee where emp_id not in(select  emp_id  from employee_salary) 
    order by id_code";
?>
<?php
    $result = mysqli_query($dbh, $sqlquery);
    while ($row = mysqli_fetch_array($result)) {
        $id_code = $row['id_code'];
        $emp_id = $row['emp_id'];

        echo "$('#emp_drp').append('<option value='$emp_id'> $id_code</option>')";
    }
}
else if ($type == "department_id") {
    $sqlquery = "SELECT department_id,department_name FROM departmentmaster order by department_name";
?>
<?php
    $result = mysqli_query($dbh, $sqlquery);
    while ($row = mysqli_fetch_array($result)) {
        $department_id = $row['department_id'];
        $department_name = $row['department_name'];

        echo "$('#department_id').append('<option value='$department_id'> $department_name</option>')";
    }
}
else if ($type == "designation_id") {
    $sqlquery = "SELECT designation_id,designation_name FROM designationmaster order by designation_name";
?>
<?php
    $result = mysqli_query($dbh, $sqlquery);
    while ($row = mysqli_fetch_array($result)) {
        $designation_id = $row['designation_id'];
        $designation_name = $row['designation_name'];

        echo "$('#designation_id').append('<option value='$designation_id'> $designation_name</option>')";
    }
}
else if ($type == "shift_id") {
    $sqlquery = "SELECT shift_id,shift_no,shift_name FROM shift_master order by shift_no";
?>
<?php
    $result = mysqli_query($dbh, $sqlquery);
    while ($row = mysqli_fetch_array($result)) {
        $shift_id = $row['shift_id'];
        $shift_no = $row['shift_no'];

        echo "$('#shift_id').append('<option value='$shift_id'> $shift_no</option>')";
    }
}
else if ($type == "bank_id") {
    $sqlquery = "SELECT bank_id,bank_name FROM bank_master order by bank_name";
?>
<?php
    $result = mysqli_query($dbh, $sqlquery);
    while ($row = mysqli_fetch_array($result)) {
        $bank_id = $row['bank_id'];
        $bank_name = $row['bank_name'];

        echo "$('#bank_id').append('<option value='$bank_id'> $bank_name</option>')";
    }
}
?>