<?php include("../../connectionsettings.inc"); ?>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Emp Code</th>
            <th>Department</th>
            <th>Designation</th>
            <th>Shift</th>
            <th>Bank</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $qry = "SELECT `emp_id`,
        (select id_code FROM `employee` where emp_id=emp_sal.emp_id) as id_code,
         `emp_salary_id`, `progress_id`, grosssalary,basic,da,hra,conveyance
         ,medicalallowance,specialallowance,otherallowances,department_id,
         (select department_name FROM `departmentmaster` where department_id=emp_sal.department_id) as department_name,
        designation_id,
        (select designation_name FROM `designationmaster` where designation_id=emp_sal.designation_id)as designation_name,
        shift_id,
        (select shift_no FROM `shift_master` where shift_id=emp_sal.shift_id) as shift_no,
        bank_id,
        (select bank_name FROM `bank_master` where bank_id=emp_sal.bank_id)as bank_name,
        account_number FROM `employee_salary` emp_sal";
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {
        ?>
            <tr>
                <td><?php echo $row['id_code']; ?> </td>
                <td><?php echo $row['department_name']; ?></td>
                <td><?php echo $row['designation_name']; ?></td>
                <td><?php echo $row['shift_no']; ?></td>
                <td><?php echo $row['bank_name']; ?></td>
                <td> <button class="btn btn-info btn-sm" onclick="editid('<?php echo $row['emp_salary_id']; ?>',
                    '<?php echo $row['grosssalary']; ?>', '<?php echo $row['basic']; ?>',
                    '<?php echo $row['da']; ?>', '<?php echo $row['hra']; ?>', '<?php echo $row['conveyance']; ?>',
                    '<?php echo $row['medicalallowance']; ?>','<?php echo $row['specialallowance']; ?>',
                    '<?php echo $row['otherallowances']; ?>', '<?php echo $row['department_id']; ?>', '<?php echo $row['designation_id']; ?>',
                    '<?php echo $row['shift_id']; ?>', '<?php echo $row['bank_id']; ?>', '<?php echo $row['account_number']; ?>')">
                        <i class="fas fa-pencil-alt">
                        </i>
                        Edit
                    </button>
                    <!-- <span><a alt="Delete" href="javascript:removeemployee(<?php echo $row['department_id']; ?>)"><button type="button" class="btn btn-danger btn-sm">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button></a></span> -->
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>