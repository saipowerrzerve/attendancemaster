<?php include("../../connectionsettings.inc"); ?>
<?php
$id = $_POST['data'];
$type = $_POST['type'];
$emp_drp_dt = $_POST['emp_drp_dt'];
$department_id_dt = $_POST['department_id_dt'];
$designation_id_dt = $_POST['designation_id_dt'];
$shift_id_dt = $_POST['shift_id_dt'];
$bank_id_dt = $_POST['bank_id_dt'];
$grosssalary_dt = $_POST['grosssalary_dt'];
$basic_dt = $_POST['basic_dt'];
$da_dt = $_POST['da_dt'];
$hra_dt = $_POST['hra_dt'];
$conveyance_dt = $_POST['conveyance_dt'];
$medicalallowance_dt = $_POST['medicalallowance_dt'];
$specialallowance_dt = $_POST['specialallowance_dt'];
$otherallowances_dt = $_POST['otherallowances_dt'];
$account_number_dt = $_POST['account_number_dt'];

if ($type == "save" && $id == 0) {
    $chkqry = "SELECT COALESCE(MAX(progress_id),0)+1 as progress_id  fROM employee_salary";
    $result = mysqli_query($dbh, $chkqry);
    $row = mysqli_fetch_array($result);
    $progress_id = $row[0];

    $dbh->begin_transaction();
    try {
        // A set of queries; if one fails, an exception should be thrown
        $dbh->query(" INSERT INTO `employee_salary`
        (`emp_id`,`progress_id`,`grosssalary`,
        `basic`,`da`,`hra`,`conveyance`,`medicalallowance`,
        `specialallowance`,`otherallowances`,`department_id`,
        `designation_id`,`shift_id`,`bank_id`,`account_number`)
        VALUES ('$emp_drp_dt','$progress_id','$grosssalary_dt',
        $basic_dt,'$da_dt','$hra_dt','$conveyance_dt','$medicalallowance_dt',
        '$specialallowance_dt','$otherallowances_dt','$department_id_dt',
        '$designation_id_dt','$shift_id_dt','$bank_id_dt','$account_number_dt')");

        $emp_salary_id = $dbh->insert_id;

        $dbh->query("INSERT INTO `attendance`.`employee_progress`
       (`emp_id`,`progress_id`,`currentactive`,`department_id`,`designation_id`,
       `start_date`,`end_date`,`shift_id`,`bank_id`,`account_number`,`grosssalary`,
       `basic`, `da`, `hra`,`conveyance`,`medicalallowance`,`specialallowance`,
       `otherallowances`,`emp_salary_id`)
       VALUES
       ('$emp_drp_dt','$progress_id',1,'$department_id_dt','$designation_id_dt'
        ,NOW(),NOW(),'$shift_id_dt','$bank_id_dt','$account_number_dt','$grosssalary_dt'
        ,'$basic_dt','$da_dt','$hra_dt','$conveyance_dt','$medicalallowance_dt','$specialallowance_dt'
        ,'$otherallowances_dt','$emp_salary_id');");

        $dbh->commit();
        if ($dbh) {
            echo 1;
        }
    } catch (mysqli_sql_exception $exception) {
        $dbh->rollback();
        throw $exception;
        echo 0;
    }
} elseif ($type == "update" && $id != 0) {
    $chkqry = "SELECT COALESCE(MAX(progress_id),0)+1 as progress_id  fROM employee_progress";
    $result = mysqli_query($dbh, $chkqry);
    $row = mysqli_fetch_array($result);
    $progress_id = $row[0];

    $dbh->begin_transaction();
    try {
        // A set of queries; if one fails, an exception should be thrown
        $dbh->query("UPDATE `employee_salary` SET 
        grosssalary = '$grosssalary_dt',basic='$basic_dt',da='$da_dt',hra='$hra_dt',
        conveyance='$conveyance_dt',medicalallowance='$medicalallowance_dt',
        specialallowance='$specialallowance_dt',otherallowances='$otherallowances_dt',
        department_id='$department_id_dt',designation_id='$designation_id_dt',
        shift_id='$shift_id_dt',bank_id='$bank_id_dt',account_number='$account_number_dt'
        WHERE emp_salary_id=$id");

        $dbh->query("UPDATE `employee_progress` SET 
        currentactive = '0'  WHERE emp_salary_id=$id");

        $dbh->query("INSERT INTO `attendance`.`employee_progress`
       (`emp_id`,`progress_id`,`currentactive`,`department_id`,`designation_id`,
       `start_date`,`end_date`,`shift_id`,`bank_id`,`account_number`,`grosssalary`,
       `basic`, `da`, `hra`,`conveyance`,`medicalallowance`,`specialallowance`,
       `otherallowances`,`emp_salary_id`)
       VALUES
       ('$emp_drp_dt','$progress_id',1,'$department_id_dt','$designation_id_dt'
        ,NOW(),NOW(),'$shift_id_dt','$bank_id_dt','$account_number_dt','$grosssalary_dt'
        ,'$basic_dt','$da_dt','$hra_dt','$conveyance_dt','$medicalallowance_dt','$specialallowance_dt'
        ,'$otherallowances_dt','$id');");

        $dbh->commit();
        if ($dbh) {
            echo 1;
        }
    } catch (mysqli_sql_exception $exception) {
        $dbh->rollback();
        throw $exception;
        echo 0;
    }
}

?>
<?php mysqli_close($dbh) ?>