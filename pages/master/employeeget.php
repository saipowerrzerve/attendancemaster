<?php include("../../connectionsettings.inc");
$path = getcwd();
?>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
             <th>Images</th>
            <th>Employee Code</th>
            <th>First Name</th>
            <th>Department Name</th>
            <th>Designation Name</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $qry = "SELECT `emp_id`,`id_code`,`imageurl`,`first_name`,
emp.`department_id`,department_name,emp.`designation_id`,designation_name
 FROM `employee` emp left join departmentmaster dm on dm.department_id=emp.department_id
left join designationmaster desg on desg.designation_id=emp.designation_id
order by id_code";
        // $conn = mysqli_connect("localhost", "root", "spz", "attendance");
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {

        ?>
            <tr>
                <!-- // $path . '/emp_images/' . $id_code . '.jpg'; -->
                <td>
                <?php echo "<div class='user-block'><img id='tstimage' 
                class='user-block img-circle img-bordered-lg' src='$row[imageurl]' 
                alt='User Image'/></div>"?>
                </td>
                <td><?php echo $row['id_code'] ?></td>
                <td><?php echo $row['first_name'] ?></td>
                <td><?php echo $row['department_name'] ?></td>
                <td><?php echo $row['designation_name'] ?></td>
                <td> <span><a alt="Edit" href="javascript:editid('<?php echo $row['emp_id'] ?>')">
                            <button class="btn btn-info btn-sm">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button></a></span>
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table> 
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"],
          
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });
</script>