<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>Menu Master</title>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4 id="head">Menu Master<small class="text-muted">Table</small></h4>
                </div>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add Details</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <!-- ----------------------FORM--------------------------------- -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info" id="form" hidden>
                        <div class="card-header">
                            <h3 class="card-title">Menu Form</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">MenuName</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="menuname" name="menuname" placeholder="Enter Menu  Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Menu Url</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="menuurl" name="menuurl" placeholder="Enter Menu Url">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Priority</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="priority" name="priority" placeholder="Enter Priority">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Menu Path</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="menupath" name="menupath" placeholder="Enter Menu Path">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Menu Header</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="menuheader" name="menuheader" placeholder="Enter Menu Header">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Parent Id</label>
                                    <div class="col-sm-10">
                                    <select class="form-control" id="parentid" name="parentid" placeholder="Enter Parent Id">
                                            <option value="1">Master</option>
                                            <option value="2">Menu</option>
                                            <option value="3">Notification</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Display Icon</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="displayicon" name="displayicon" placeholder="Enter Display Icon">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class=row>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-success float-right" onclick="save()"><i class="fas fa-save">
                                            </i>
                                            Save</button>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-danger float-left" id="cancel">
                                            <i class="fas fa-times-circle"></i>
                                            <span>Cancel</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                    <!-- ------------------DATA TABLE------------------------- -->
                    <div class="card card-info" id="dt">
                        <div class="card-header">
                            <h3 class="card-title">Menu Details</h3>
                        </div>
                        <div class="loaderclass" hidden>
                        </div>
                        <!-- /.card-header -->
                        <div class="divclass">
                            <div class="card-body" id="menu_dt">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<script>
    var menuid = 0;
    $(document).ready(function() {
        $("#additem").click(function() {
            $("#form").show().removeAttr('hidden');
            $("#dt").hide();
            $("#additem").hide();
            $("#head").hide();
            clear();
            menuid = 0;

        });
        $("#cancel").click(function() {
            $("#dt").show();
            $("#form").hide();
            $("#additem").show();
            $("#head").show();
            clear();
        });
        datatable();
    });
    // --------------------------------------DATA TABLE CALLED-------------------
    function datatable() {
        //DATA TABLE CALLED
        loaderstart("divclass", "loaderclass");
        $.ajax({
            type: "POST",
            url: "menumasterget.php",
            success: function(response) {
                $("#menu_dt").html(response);
                loaderend("divclass", "loaderclass");
            }
        })
        $("#form").hide();
        $("#dt").show();
        $("#additem").show();
        $("#head").show();
    }
    //--------------------------------------SAVE FUNCTION------------------
    function save() {
        //SAVE
        var type = "";
        if (menuid == 0) {
            type = "save"
        } else {
            type = "update"
        }
        var menuname = $('#menuname').val();
        var menuurl = $('#menuurl').val();
        var priority = $('#priority').val();
        var menupath = $('#menupath').val();
        var menuheader = $('#menuheader').val();
        var parentid = $('#parentid').val();
        var displayicon = $('#displayicon').val();
        if (menuname == "" || menuname == null) {
            statusswal("Menuname Name must be filled out", 'warning');
            return false;
        }
        if (menuurl == "" || menuurl == null) {
            statusswal("MenuUrl must be filled out", 'warning');
            return false;
        }
        if (priority == "" || priority == null) {
            statusswal("Priority must be filled out", 'warning');
            return false;
        }
        if (menupath == "" || menupath == null) {
            statusswal("Menupath must be filled out", 'warning');
            return false;
        }
        if (menuheader == "" || menuheader == null) {
            statusswal("Menuheader must be filled out", 'warning');
            return false;
        }
        if (parentid == "" || parentid == null) {
            statusswal("Parent Id must be filled out", 'warning');
            return false;
        }
        if (displayicon == "" || displayicon == null) {
            statusswal("DisplayIcon must be filled out", 'warning');
            return false;
        }

        $.ajax({
            type: "POST",
            data: {
                data: menuid,
                type: type,
                menuname: menuname,
                menuurl: menuurl,
                priority: priority,
                menupath: menupath,
                menuheader: menuheader,
                parentid: parentid,
                displayicon: displayicon,
            },
            url: "../master/menumasterset.php",
            success: function(response) {
                if (response == "0") {
                    if (menuid == 0) {
                        statusswal("Menumaster failed to Create", 'error');
                    } else {
                        statusswal('Menumaster failed to Update', 'error');
                    }

                } else {
                    if (menuid == 0) {
                        statusswal("Menumaster created successfully", 'success');
                    } else {
                        statusswal("Menumaster Updated successfully", 'success');
                    }
                    if (response == "2") {
                        statusswal("Menu name Already Exist", 'error');
                    }

                    datatable();
                }
            }

        })

    }
    //---------------------------------------EDIT FUNCTION-----------------
    function editid(id) {
        //EDIT
        menuid = id;
        var type = "edit";
        $("#dt").hide();
        $("#form").show().removeAttr('hidden');
        $.ajax({
            type: "POST",
            data: {
                "data": menuid,
                "type": type
            },
            url: "../master/menumasterset.php",
            success: function(response) {
                var json = $.parseJSON(response);
                $('#menuname').val(json[0].menuname),
                    $('#menuurl').val(json[0].menuurl),
                    $('#priority').val(json[0].priority),
                    $('#menupath').val(json[0].menupath),
                    $('#menuheader').val(json[0].menuheader),
                    $('#parentid').val(json[0].parentid),
                    $('#displayicon').val(json[0].displayicon)
            }
        })
    }
    // function removeemployee(id) {
    //     //DELETE
    //     menuid = id;
    //     var type = "";
    //     if (menuid != "" && menuid != null) {
    //         type = "delete"
    //     }
    //     $.ajax({
    //         type: "POST",
    //         data: {
    //             "data": menuid,
    //             "type": type
    //         },
    //         url: "../master/menumasterset.php",
    //         success: function(dat) {
    //             alert(dat);
    //             datatable();
    //         }
    //     })
    // }
    //-----------------------------------CLEAR FUNCTION---------------------
    function clear() {
        $('#menuname').val(''),
            $('#menuurl').val(''),
            $('#priority').val(''),
            $('#menupath').val(''),
            $('#menuheader').val(''),
            $('#parentid').val(''),
            $('#displayicon').val('')
    }
</script>
<!-- /.content-wrapper -->
<?php include("../../include/footer.php"); ?>