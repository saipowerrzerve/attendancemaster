<?php include("../../connectionsettings.inc"); ?>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Menu Name</th>
            <th>Menu Url</th>
            <th>Priority</th>
            <th>Menu Path</th>
            <th>Menu Header</th>
            <th>Parent Id</th>
            <th>Display Icon</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $qry = "SELECT `menuid`, `menuname`, `menuurl`, `priority`, 
                        `menupath`, `menuheader`, `parentid`, `displayicon` FROM 
                        `menumaster`";
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {
        ?>
            <tr>
                <td><?php echo $row['menuname']; ?> </td>
                <td><?php echo $row['menuurl']; ?></td>
                <td><?php echo $row['priority']; ?></td>
                <td><?php echo $row['menupath']; ?></td>
                <td><?php echo $row['menuheader']; ?></td>
                <td><?php echo $row['parentid']; ?></td>
                <td><?php echo $row['displayicon']; ?></td>
                <td> <span><a alt="Edit" href="javascript:editid(<?php echo $row['menuid']; ?> )"><button class="btn btn-info btn-sm">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button></a></span>
                    <!-- <span> <a alt="Delete" href="javascript:removeemployee(<?php echo $row['menuid']; ?>)"><button class="btn btn-danger btn-sm">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button></a></span> -->
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>