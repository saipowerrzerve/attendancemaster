<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>Menu User Rights</title>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid " id="add">
            <div class="row">
                <div class="col-sm-6">
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content mt-1">
        <div class="container-fluid" id="table">
            <div class="row">
                <div class="col-12">
                    <div class="callout callout-info">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>
                                    <i class="fas fa-user" style="color: #335e8a;"></i> Users Menu
                                </h4>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group mt-0">
                                            <?php
                                            $sqlquery = "SELECT um.userid,emp.id_code FROM usermaster um inner join employee emp on emp.emp_id=um.emp_id order by emp.id_code";
                                            ?>
                                            <!-- <label>User</label> -->
                                            <select class="form-control select2bs4" id="usr_drp" style="width: 100%;">
                                                <?php
                                                $result = mysqli_query($dbh, $sqlquery);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    $userid = $row['userid'];
                                                    $emp_id = $row['id_code'];
                                                ?>
                                                    <option value="<?php echo $userid; ?>"><?php echo $emp_id; ?></option>
                                                <?php } ?>
                                            </select>

                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn bg-gradient-info btn-sm float-left mt-1" id="search" onclick="datatable()"><i class="fas fa-search"> </i>get Details </button>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="button" class="btn bg-gradient-success btn-sm float-right mt-1" id="save" onclick="save()"><i class="fas fa-save"></i> Save</button>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <div class="invoice p-3 mb-3">
                        <!-- Table row -->
                        <div class="loaderclass" hidden>

                        </div>
                        <div class="divclass">
                            <div class="row">
                                <div class="col-12 table-responsive" id="usr_menu_prv">
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    var designation_id = 0;
    $(document).ready(function() {
        datatable();

    });
    // -------------------------------------table---------------------------
    function datatable() {
        $('#search').attr('disabled', 'disabled');
        loaderstart("divclass", "loaderclass");
        var useridval = $("#usr_drp").val()
        $.ajax({
            type: "GET",
            url: "menuuserrightsget.php",
            data: {
                userid: useridval
            },
            success: function(response) {
                $("#usr_menu_prv").html(response);
                $('#search').removeAttr('disabled');
                loaderend("divclass", "loaderclass");
            }
        })
    }
    // -------------------------------------end---------------------------
    function save() {
        $('#save').attr('disabled', 'disabled');
        var useridval = $("#usr_drp").val()
        if (useridval == "" || useridval == null) {
            Swal.fire("", "username name must be filled out", 'warning')
            return false;
        }
        var Details = [];
        var MenuIds = [];
        var items = document.getElementsByClassName('checks');
        for (var i = 0; i < items.length; i++) {
            MenuIds[i] = items[i].value;
        }
        if ((MenuIds.length) > 0) {
            for (var i = 0; i < MenuIds.length; i++) {
                var menuval = "0";
                var isadminval = "0";
                if ($('#menu_' + MenuIds[i]).prop('checked')) {
                    menuval = MenuIds[i];
                }
                if ($('#isadmin_' + MenuIds[i]).prop('checked')) {
                    isadminval = "1";
                }
                var obj = {
                    'menu': menuval,
                    'isadmin': isadminval,
                }
                Details.push(obj);
            }
        }
        console.log(Details)
        //  else {
        //     var obj = {
        //         'menu': '0',
        //         'userid': $('#usr_drp').val()
        //     }
    //     Details.push(obj);
    // }
    $.ajax({
        type: "POST",
        data: {
            "usr_details": Details,
            "userid": useridval,
        },
        url: "menuuserrightsset.php",
        success: function(response) {
            if (response == "0") {
                statusswal('Usermenu assigned failed', 'error');
            } else {
                statusswal('Usermenu assigned successfully', 'success');
                datatable();
            }
            $('#save').removeAttr('disabled');
        }
    })
    }
</script>
<?php include("../../include/footer.php"); ?>