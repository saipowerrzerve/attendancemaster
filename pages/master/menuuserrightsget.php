<?php include("../../connectionsettings.inc");
$userid = $_GET['userid'];
?>
<table id="usr_menu_tab" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>
                <!-- <input type="checkbox" id="chkselectall" onchange="Selectall()"> -->
                <div class="icheck-primary d-inline">
                    <input type="checkbox" id="chkselectall" onchange="Selectall()">
                    <label for="chkselectall">
                    </label>
                </div>
            </th>
            <th>Menuname</th>
            <th>menu</th>
            <th>isadmin</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $qry = "SELECT   mur.userid,mm.menuname,mm.`menuid`, `isadmin` FROM `menumaster` mm  left join menuuserrights mur on mur.menuid=mm.menuid
        and mur.userid=$userid  order by mm.menuid";
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        $counter = 1;
        if ($count == 0) {
        ?>
            <?php
        } else {

            while ($row = mysqli_fetch_array($result)) {
                $menuid = "";
                $isadmin = "";
                if ($row['menuid']&&$row['userid']) {
                    $viewdetails = "checked";
                }
                else{
                    $viewdetails = "";
                }
                if ($row['isadmin'] == 1) {
                    $admindetails = "checked";
                }
                else{
                    $admindetails = "";
                }
            ?>
                <tr>
                    <td><?php echo $counter; ?> </td>
                    <td><input type="checkbox" hidden id="m_<?php echo $row['menuid']; ?>" value="<?php echo $row['menuid']; ?>" class='checks' style="width: 100px;">
                        <span><?php echo $row['menuname']; ?></span>
                    </td>
                    <td>
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="menu_<?php echo $row['menuid']; ?>" <?php echo $viewdetails ?>>
                            <label for="menu_<?php echo $row['menuid']; ?>">
                            </label>
                        </div>
                    </td>
                    <td>
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="isadmin_<?php echo $row['menuid']; ?>" <?php echo $admindetails ?>>
                            <label for="isadmin_<?php echo $row['menuid']; ?>">
                            </label>
                        </div>
                    </td>
                </tr>

            <?php
                $counter++;
            } ?>
        <?php
        } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#usr_menu_tab").DataTable({
            "lengthMenu": [
                [-1],
                ["All"]
            ],
            "responsive": true,
            //"lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"],
        }).buttons().container().appendTo('#usr_menu_tab_wrapper .col-md-6:eq(0)');
    });
//-------------------------------------------CHECK BOX Selectall---------------------------------
    function Selectall() {
        var chk = $("#chkselectall").is(":checked");
        var items = document.getElementsByClassName('checks');

        for (var i = 0; i < items.length; i++) {
            if (chk) {
                $('#menu_' + items[i].value).prop('checked', true);
                $('#isadmin_' + items[i].value).prop('checked', true);
            } else {
                $('#menu_' + items[i].value).prop('checked', false);
                $('#isadmin_' + items[i].value).prop('checked', false);
            }
        }
    }
</script>