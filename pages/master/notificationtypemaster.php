<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>Notification Type</title>
<!-- Content Wrapper. Contains page content -->
<!-- ---------------------------------------FORM------------------------------- -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4 id="head">Notification Type
                        <small class="text-muted">Table</small>
                    </h4>
                </div>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add Details</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info" id="form" hidden>
                        <div class="card-header">
                            <h3 class="card-title">Notification Type Form</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Notification name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="notificationname" name="notificationname" placeholder="Enter Notification Name">
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class=row>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-success float-right" onclick="save()"><i class="fas fa-save">
                                            </i>
                                            Save</button>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-danger float-left" id="cancel">
                                            <i class="fas fa-times-circle"></i>
                                            <span>Cancel</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                    <!-- --------------------------------DATA TABLE --------------->
                    <div class="card card-info" id="dt">
                        <div class="card-header">
                            <h3 class="card-title">Notification Details</h3>
                        </div>
                        <div class="loaderclass" hidden>
                        </div>
                        <!-- /.card-header -->
                        <div class="divclass">
                            <div class="card-body" id="not_dt">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<script>
    var not_id = 0;
    $(document).ready(function() {
        $("#additem").click(function() {
            $("#form").show().removeAttr('hidden');
            $("#dt").hide();
            $("#additem").hide();
            $("#head").hide();
            clear();
            not_id = 0;
        });
        $("#cancel").click(function() {
            $("#dt").show();
            $("#form").hide();
            $("#additem").show();
            $("#head").show();
            clear();
        });
        datatable();
    });
    //----------------------------------DATA TABLE CALLED----------------------
    function datatable() {
        //DATA TABLE CALLED
        loaderstart("divclass", "loaderclass");
        $.ajax({
            type: "POST",
            url: "notificationtypemasterget.php",
            success: function(response) {
                $("#not_dt").html(response);
                loaderend("divclass", "loaderclass");
            }
        })
        $("#form").hide();
        $("#dt").show();
        $("#additem").show();
        $("#head").show();

    }
    //---------------------------SAVE---------------------------
    function save() {
        //save
        var type = "";
        if (not_id == 0) {
            type = "save"
        } else {
            type = "update"
        }
        var notificationname = $('#notificationname').val();
        if (notificationname == "" || notificationname == null) {
            statusswal("NotificationName must be filled out", 'warning');
            return false;
        }
        $.ajax({
            type: "POST",
            data: {
                data: not_id,
                type: type,
                notificationname: notificationname,
            },
            url: "../master/notificationtypemasterset.php",
            success: function(response) {
                if (response == "0") {
                    if (not_id == 0) {
                        statusswal("Notification type Name failed to Create", 'error');
                    } else {
                        statusswal("Notification type Name failed to Update", 'error');
                    }

                } else {
                    if (not_id == 0) {
                        statusswal("Notification type Name created successfully", 'success');
                    } else {
                        statusswal("Notification type Name Updated successfully", 'success');
                    }
                    if (response == "2") {
                        statusswal("Notification type name Already Exist", 'error');
                    }

                    datatable();
                }
            }
        })
    }
    //---------------------------EDIT---------------------------------
    function editid(id) {
        //edit
        not_id = id;
        var type = "edit";
        $("#dt").hide();
        $("#additem").hide();
        $("#head").hide();
        $("#form").show().removeAttr('hidden');
        $.ajax({
            type: "POST",
            data: {
                "data": not_id,
                "type": type
            },
            url: "../master/notificationtypemasterset.php",
            success: function(response) {
                var json = $.parseJSON(response);
                $('#notificationname').val(json[0].notificationname);
            }
        })
    }
    // function removeemployee(id) {
    //     //delete
    //     not_id = id;
    //     var type = "";
    //     if (not_id != "" && not_id != null) {
    //         type = "delete"
    //     }
    //     $.ajax({
    //         type: "POST",
    //         data: {
    //             "data": not_id,
    //             "type": type
    //         },
    //         url: "../master/notificationtypemasterset.php",
    //         success: function(dat) {
    //             alert(dat);
    //             datatable();
    //         }
    //     })
    // }
    //----------------------------------CLEAR--------------------------
    function clear() {
        $('#notificationname').val('');
    }
</script>
<?php include("../../include/footer.php"); ?>