<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>Notification Users</title>
<!-- Content Wrapper. Contains page content -->
<!-- ---------------------------------------FORM------------------------------- -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4 id="head">Notification Users
                        <small class="text-muted">Table</small>
                    </h4>
                </div>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add Details</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info" id="form" hidden>
                        <div class="card-header">
                            <h3 class="card-title">Notification User Form</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">User Id</label>
                                    <div class="col-sm-10">
                                        <div class="select2-purple">
                                            <select class="select2" multiple="multiple" id="userid" data-placeholder="Select User Id" data-dropdown-css-class="select2-purple" style="width: 100%;">
                                                <?php
                                                $sql = "select employee.id_code,employee.first_name from employee INNER JOIN usermaster on usermaster.emp_id=employee.emp_id;";
                                                $result = mysqli_query($dbh, $sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                ?>
                                                    <option value="<?php echo $row['id_code']; ?>"><?php echo $row['id_code']; ?>-<?php echo $row['first_name']; ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Circular Id</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="circularid" name="circularid" placeholder="Enter Circular Id">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class=row>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-success float-right" onclick="save()"><i class="fas fa-save">
                                            </i>
                                            Save</button>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-danger float-left" id="cancel">
                                            <i class="fas fa-times-circle"></i>
                                            <span>Cancel</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                    <!-- --------------------------------DATA TABLE --------------->
                    <div class="card card-info" id="dt">
                        <div class="card-header">
                            <h3 class="card-title">Notification Users Details</h3>
                        </div>
                        <div class="loaderclass" hidden>
                        </div>
                        <!-- /.card-header -->
                        <div class="divclass">
                            <div class="card-body" id="not_dt">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<script>
    $(function() {
        $('.select2').select2();
    })
    var nt = 0;
    $(document).ready(function() {
        $("#additem").click(function() {
            $("#form").show().removeAttr('hidden');
            $("#dt").hide();
            $("#additem").hide();
            $("#head").hide();
            clear();
            nt = 0;
        });
        $("#cancel").click(function() {
            $("#dt").show();
            $("#form").hide();
            $("#additem").show();
            $("#head").show();
            clear();
        });
        datatable();
    });
    //-------------------------------------------DATA TABLE CALLED----------------------------
    function datatable() {
        //DATA TABLE CALLED
        loaderstart("divclass", "loaderclass");
        $.ajax({
            type: "POST",
            url: "notificationusersget.php",
            success: function(response) {
                $("#not_dt").html(response);
                loaderend("divclass", "loaderclass");
            }
        })
        $("#form").hide();
        $("#dt").show();
        $("#additem").show();
        $("#head").show();
    }
    //-----------------------------SAVE-------------------------
    function save() {
        //save
        var type = "";
        if (nt == 0) {
            type = "save"
        }
        var userid = [];
        $.each($("#userid option:selected"), function() {
            userid.push($(this).val());
        });
        $.ajax({
            type: "POST",
            data: {
                data: nt,
                type: type,
                userid: userid,
                circularid: $('#circularid').val(),
            },
            url: "../master/notificationusersset.php",
            success: function(response) {

                if (response == "0") {
                    statusswal("Notification failed to Create", 'error');
                } else {
                    statusswal("Notification created successfully", 'success');
                    datatable();
                }

            }
        })
    }
    //---------------------------------------DELETE-----------------------------
    function removeemployee(id) {
        //delete
        nt = id;
        var type = "";
        if (nt != "" && nt != null) {
            type = "delete"
        }
        $.ajax({
            type: "POST",
            data: {
                "data": nt,
                "type": type
            },
            url: "../master/notificationusersset.php",
            success: function(response) {
                if (response == "2") {
                    statusswal("Notification failed to delete", 'error');
                } else {
                    statusswal("Notification deleted successfully", 'success');
                    datatable();
                }
            }
        })
    }
    //--------------------------------CLEAR-------------------------------------------
    function clear() {
        //CLEAR
        // $('#userid').val('');
        $('#userid').val(null).trigger('change');
        $('#circularid').val('');
    }
</script>
<?php include("../../include/footer.php"); ?>