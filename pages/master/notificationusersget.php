<?php include("../../connectionsettings.inc"); ?>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>User Id</th>
            <th>circular Id</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
          $yy = substr(date('Y'),2,3); //from 2021 it is taking 21
          // echo $d;
          // $yy=21; 
          $qry = "SELECT `userid`, `circularid` FROM `notification_users_$yy`  "; 
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {
        ?>
            <tr>
                <td><?php echo $row['userid']; ?> </td>
                <td><?php echo $row['circularid']; ?> </td>
                <td>
                    
                    <span> <a alt="Delete" href="javascript:removeemployee(<?php echo $row['circularid']; ?>)"><button class="btn btn-danger btn-sm">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button></a></span>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>