<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>ShiftHours</title>
<!-- Content Wrapper. Contains page content -->
<!-- ---------------------------------------FORM------------------------------- -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4 id="head">Shift Hours<small class="text-muted">Table</small></h4>
                </div>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add Details</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info" id="form" hidden>
                        <div class="card-header">
                            <h3 class="card-title">Shift Hours Form</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Shift Number</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="shiftno" name="shiftno" placeholder="Enter Shift Number">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Hour Number</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="hourno" name="hourno" placeholder="Enter Hour Number">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Next Shift</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="nextshift" name="nextshift" placeholder="Enter Next Shift">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Previous Shift</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="previousshift" name="previousshift" placeholder="Enter Previous Shift">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Next Hour</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="nexthour" name="nexthour" placeholder="Enter Next Hour">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Previous Hour</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="previoushour" name="previoushour" placeholder="Enter Previous Hour">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Start Hour</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="starthour" name="starthour" placeholder="Enter Start Hour">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">End Hour</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="endhour" name="endhour" placeholder="Enter End Hour">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class=row>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-success float-right" onclick="save()"><i class="fas fa-save">
                                            </i>
                                            Save</button>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-danger float-left" id="cancel">
                                            <i class="fas fa-times-circle"></i>
                                            <span>Cancel</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                    <!-- --------------------------------DATA TABLE --------------->
                    <div class="card card-info" id="dt">
                        <div class="card-header">
                            <h3 class="card-title">Shift Hours Details</h3>
                        </div>
                        <div class="loaderclass" hidden>
                        </div>
                        <!-- /.card-header -->
                        <div class="divclass">

                            <div class="card-body" id="shifthr_dt">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<script>
    var sh = 0;
    $(document).ready(function() {
        $("#additem").click(function() {
            $("#form").show().removeAttr('hidden');
            $("#dt").hide();
            $("#additem").hide();
            $("#head").hide();
            clear();
            sh = 0;

        });
        $("#cancel").click(function() {
            $("#dt").show();
            $("#form").hide();
            $("#additem").show();
            $("#head").show();
            clear();
        });
        datatable();
    });
    //-------------------------DATA TABLE------------------------------------
    function datatable() {
        //DATA TABLE CALLED
        loaderstart("divclass", "loaderclass");
        $.ajax({
            type: "POST",
            url: "shifthoursget.php",
            success: function(response) {
                $("#shifthr_dt").html(response);
                loaderend("divclass", "loaderclass");
            }
        })
        $("#form").hide();
        $("#dt").show();
        $("#additem").show();
        $("#head").show();
    }
    //--------------------------------SAVE-----------------------------------
    function save() {
        //save
        var type = "";
        if (sh == 0) {
            type = "save"
        } else {
            type = "update"
        }
        var shiftno = $('#shiftno').val();
        var hourno = $('#hourno').val();
        var nextshift = $('#nextshift').val();
        var previousshift = $('#previousshift').val();
        var nexthour = $('#nexthour').val();
        var previoushour = $('#previoushour').val();
        var starthour = $('#starthour').val();
        var endhour = $('#endhour').val();

        if (shiftno == "" || shiftno == null) {
            statusswal("Shift Number must be filled out", 'warning');
            return false;
        }
        if (hourno == "" || hourno == null) {
            statusswal("Hour Number must be filled out", 'warning');
            return false;
        }
        if (nextshift == "" || nextshift == null) {
            statusswal("Next Shift must be filled out", 'warning');
            return false;
        }
        if (previousshift == "" || previousshift == null) {
            statusswal("Previousshift must be filled out", 'warning');
            return false;
        }
        if (nexthour == "" || nexthour == null) {
            statusswal("Next Hour must be filled out", 'warning');
            return false;
        }
        if (previoushour == "" || previoushour == null) {
            statusswal("Previous Hour must be filled out", 'warning');
            return false;
        }
        if (starthour == "" || starthour == null) {
            statusswal("Start Hour must be filled out", 'warning');
            return false;
        }
        if (endhour == "" || endhour == null) {
            statusswal("End Hour must be filled out", 'warning');
            return false;
        }
        $.ajax({
            type: "POST",
            data: {
                data: sh,
                type: type,
                shiftno: shiftno,
                hourno: hourno,
                nextshift: nextshift,
                previousshift: previousshift,
                nexthour: nexthour,
                previoushour: previoushour,
                starthour: starthour,
                endhour: endhour,

            },
            url: "../master/shifthoursset.php",
            success: function(response) {
                if (response == "0") {
                    if (sh == 0) {
                        statusswal('Shift Hour failed to Create', 'error');
                    } else {
                        statusswal("Shift Hour failed to Update", 'error');
                    }

                } else {
                    if (sh == 0) {
                        statusswal("Shift Hour created successfully", 'success');
                    } else {
                        statusswal("Shift Hour Updated successfully", 'success');
                    }


                    datatable();
                }
            }
        })
    }
    //-------------------------------------------EDIT-------------------------
    function editid(id) {
        //edit
        sh = id;
        var type = "edit";
        $("#dt").hide();
        $("#additem").hide();
        $("#form").show().removeAttr('hidden');
        $.ajax({
            type: "POST",
            data: {
                "data": sh,
                "type": type
            },
            url: "../master/shifthoursset.php",
            success: function(response) {
                var json = $.parseJSON(response);
                console.log(json)
                $('#shiftno').val(json[0].shift_no);
                $('#hourno').val(json[0].hourno);
                $('#nextshift').val(json[0].nextshift);
                $('#previousshift').val(json[0].previousshift);
                $('#nexthour').val(json[0].nexthour);
                $('#previoushour').val(json[0].previoushour);
                $('#starthour').val(json[0].starthour);
                $('#endhour').val(json[0].endhour);
            }
        })
    }
    // function removeemployee(id) {
    //     //delete
    //     sh = id;
    //     var type = "";
    //     if (sh != "" && sh != null) {
    //         type = "delete"
    //     }
    //     $.ajax({
    //         type: "POST",
    //         data: {
    //             "data":sh,
    //             "type": type
    //         },
    //         url: "../master/shifthoursset.php",
    //         success: function(dat) {
    //             alert(dat);
    //             datatable();
    //         }
    //     })
    // }
    //---------------------------------------CLEAR-------------------------------
    function clear() {
        //CLEAR
        $('#shiftno').val('');
        $('#hourno').val('');
        $('#nextshift').val('');
        $('#previousshift').val('');
        $('#nexthour').val('');
        $('#previoushour').val('');
        $('#starthour').val('');
        $('#endhour').val('');
    }
</script>
<?php include("../../include/footer.php"); ?>