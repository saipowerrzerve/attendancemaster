<?php include("../../connectionsettings.inc"); ?>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Shift Number</th>
            <th>Hour Number</th>
            <th>Next Shift</th>
            <th>Previous Shift</th>
            <th>Next Hour</th>
            <th>Previous Hour</th>
            <th>Start Hour</th>
            <th>End Hour</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $qry = "SELECT `shift_no`, `hourno`, `nextshift`, `previousshift`, `nexthour`, 
    `previoushour`, `starthour`, `endhour` FROM `shift_hours`";
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {
        ?>
            <tr>
                <td><?php echo $row['shift_no']; ?> </td>
                <td><?php echo $row['hourno']; ?> </td>
                <td><?php echo $row['nextshift']; ?></td>
                <td><?php echo $row['previousshift']; ?></td>
                <td><?php echo $row['nexthour']; ?></td>
                <td><?php echo $row['previoushour']; ?></td>
                <td><?php echo $row['starthour']; ?></td>
                <td><?php echo $row['endhour']; ?></td>
                <td> <span><a alt="Edit" href="javascript:editid(<?php echo $row['shift_no']; ?> )"><button class="btn btn-info btn-sm">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button></a></span>
                    <!-- <span> <a alt="Delete" href="javascript:removeemployee(<?php //echo $row['shift_no']; ?>)"><button class="btn btn-danger btn-sm">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button></a></span> -->
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>