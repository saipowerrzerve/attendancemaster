<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>ShiftMaster</title>
<!-- Content Wrapper. Contains page content -->
<!-- ---------------------------------------FORM------------------------------- -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4 id="head">Shift Master <small class="text-muted">Table</small></h4>
                </div>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add Details</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info" id="form" hidden>
                        <div class="card-header">
                            <h3 class="card-title">Shift Entry Form</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Shift Number</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="shiftno" name="shiftno" placeholder="Enter Shift Number">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Shift Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="shiftname" name="shiftname" placeholder="Enter Shift Name">
                                    </div>
                                </div>
                                <div class="bootstrap-timepicker">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Shift Start Time</label>
                                        <div class="col-sm-10">

                                            <div class="input-group date datelt" id="starttime" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" data-target="#starttime" />
                                                <div class="input-group-append" data-target="#starttime" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                                                </div>
                                            </div>

                                            <!-- /.input group -->
                                        </div>
                                        <!-- /.form group -->
                                    </div>
                                </div>
                                <div class="bootstrap-timepicker">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Shift End Time</label>
                                        <div class="col-sm-10">

                                            <div class="input-group date datelt" id="endtime" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" data-target="#endtime" />
                                                <div class="input-group-append" data-target="#endtime" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                                                </div>
                                            </div>

                                            <!-- /.input group -->
                                        </div>
                                        <!-- /.form group -->
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Active Status</label>
                                    <!-- <div class="col-sm-10">
                                        <input type="text" class="form-control" id="isactive" name="isactive" placeholder="Enter Active Status">
                                    </div> -->

                                    <div class="col-sm-10">
                                        <select class="form-control" id="isactive" name="isactive" placeholder="Enter Active Status">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Priority</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="priority" name="priority" placeholder="Enter Priority">
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class=row>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-success float-right" onclick="save()"><i class="fas fa-save">
                                            </i>
                                            Save</button>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-danger float-left" id="cancel">
                                            <i class="fas fa-times-circle"></i>
                                            <span>Cancel</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                    <!-- --------------------------------DATA TABLE -------------------->
                    <div class="card card-info" id="dt">
                        <div class="card-header">
                            <h3 class="card-title">Shift Details</h3>
                        </div>
                        <div class="loaderclass" hidden>
                        </div>
                        <!-- /.card-header -->
                        <div class="divclass">
                            <div class="card-body" id="shift_dt">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<script>
    var smaster = 0;
    $(document).ready(function() {
        $("#additem").click(function() {
            $("#form").show().removeAttr('hidden');
            $("#dt").hide();
            $("#additem").hide();
            $("#head").hide();
            clear();
            smaster = 0;

        });
        $("#cancel").click(function() {
            $("#dt").show();
            $("#form").hide();
            $("#additem").show();
            $("#head").show();
            clear();
        });
        datatable();
    });
    //-------------------------------------DATA TABLE CALLED-------------------------
    function datatable() {
        //DATA TABLE CALLED
        loaderstart("divclass", "loaderclass");
        $.ajax({
            type: "POST",
            url: "shiftmasterget.php",
            success: function(response) {
                $("#shift_dt").html(response);
                loaderend("divclass", "loaderclass");
            }
        })
        $("#form").hide();
        $("#dt").show();
        $("#additem").show();
        $("#head").show();
    }
    //---------------SAVE----------------------------------------------------
    function save() {
        //save
        var type = "";
        if (smaster == 0) {
            type = "save"
        } else {
            type = "update"
        }
        var shiftno = $('#shiftno').val();
        var shiftname = $('#shiftname').val();
        var starttime = $("#starttime").find("input").val();
        var endtime = $("#endtime").find("input").val();
        var isactive = $('#isactive').val();
        var priority = $('#priority').val();
        if (shiftno == "" || shiftno == null) {
            statusswal("Shift Number must be filled out", 'warning');
            return false;
        }
        if (shiftname == "" || shiftname == null) {
            statusswal("Shift Name must be filled out", 'warning');
            return false;
        }
        if (starttime == "" || starttime == null) {
            statusswal("Start Time must be filled out", 'warning');
            return false;
        }
        if (endtime == "" || endtime == null) {
            statusswal("End Time must be filled out", 'warning');
            return false;
        }
        if (isactive == "" || isactive == null) {
            statusswal("Isactive must be filled out", 'warning');
            return false;
        }
        if (priority == "" || priority == null) {
            statusswal("Priority must be filled out", 'warning');
            return false;
        }
        $.ajax({
            type: "POST",
            data: {
                data: smaster,
                type: type,
                shiftno: shiftno,
                shiftname: shiftname,
                starttime: starttime,
                endtime: endtime,
                isactive: isactive,
                priority: priority,
            },
            url: "../master/shiftmasterset.php",
            success: function(response) {
                if (response == "0") {
                    if (smaster == 0) {
                        statusswal("Shift Master failed to Create", 'error');
                    } else {
                        statusswal("Shift Master failed to Update", 'error');
                    }

                } else {
                    if (smaster == 0) {
                        statusswal("Shift Master created successfully", 'success');
                    } else {
                        statusswal("Shift Master Updated successfully", 'success');
                    }
                    datatable();
                }
            }


        })

    }
    //--------------------------EDIT-------------------------------------
    function editid(id) {
        //edit
        smaster = id;
        var type = "edit";
        $("#dt").hide();
        $("#additem").hide();
        $("#form").show().removeAttr('hidden');
        $.ajax({
            type: "POST",
            data: {
                "data": smaster,
                "type": type
            },
            url: "../master/shiftmasterset.php",
            success: function(response) {
                var json = $.parseJSON(response);
                console.log(json)
                $('#shiftno').val(json[0].shift_no);
                $('#shiftname').val(json[0].shift_name);
                $('#starttime').datetimepicker();
                $('#starttime').datetimepicker('date', moment(json[0].shift_start_time, 'hh:mm'));
                $('#endtime').datetimepicker();
                $('#endtime').datetimepicker('date', moment(json[0].shift_start_time, 'hh:mm'));
                $('#isactive').val(json[0].isactive);
                $('#priority').val(json[0].priority);
            }
        })
    }
    // function removeemployee(id) {
    //     //delete
    //    smaster = id;
    //     var type = "";
    //     if (smaster != "" && smaster != null) {
    //         type = "delete"
    //     }
    //     $.ajax({
    //         type: "POST",
    //         data: {
    //             "data": smaster,
    //             "type": type
    //         },
    //         url: "../master/shiftmasterset.php",
    //         success: function(dat) {
    //             alert(dat);
    //             datatable();
    //         }
    //     })
    // }
    //------------------------CLEAR------------------------
    function clear() {
        $('#shiftno').val('');
        $('#shiftname').val('');
        // $('#starttime').datetimepicker();
        // $('#starttime').datetimepicker('date', moment(json[0].shift_start_time, 'hh:mm'));
        // $('#endtime').datetimepicker();
        // $('#endtime').datetimepicker('date', moment(json[0].shift_start_time, 'hh:mm'));
        $('#isactive').val('');
        $('#priority').val('');
    }
</script>
<?php include("../../include/footer.php"); ?>