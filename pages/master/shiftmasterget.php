<?php include("../../connectionsettings.inc"); ?>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Shift Number</th>
            <th>Shift Name</th>
            <th>Shift Start Time</th>
            <th>Shift End Time</th>
            <th>Is Active</th>
            <th>Priority</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $qry = "SELECT `shift_id`, `shift_no`, `shift_name`, `shift_start_time`, 
        `shift_end_time`, `isactive`, `priority` FROM `shift_master`";
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {
        ?>
            <tr>
                <td><?php echo $row['shift_no']; ?> </td>
                <td><?php echo $row['shift_name']; ?></td>
                <td><?php echo $row['shift_start_time']; ?></td>
                <td><?php echo $row['shift_end_time']; ?></td>
                <!-- <td><?php //echo $row['isactive']; ?></td> -->
                <td>
                <?php $v=$row['isactive'];  
                if($v==1)
                {?>
                   <span class="badge bg-success" style="font-size: 15px;">YES</span>
                   <?php
                }
                else
                {
                ?>
                <span class="badge bg-danger" style="font-size: 15px;">NO</span>
                <?php
                }
                ?>
                 </td>
                <td><?php echo $row['priority']; ?></td>
                <td> <span><a alt="Edit" href="javascript:editid(<?php echo $row['shift_id']; ?> )"><button class="btn btn-info btn-sm">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button></a></span>
                    <!-- <span> <a alt="Delete" href="javascript:removeemployee(<?php echo $row['shift_id']; ?>)"><button class="btn btn-danger btn-sm">
                                <i class="fas fa-trash">
                                </i>
                                Delete
                            </button></a></span> -->
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });
</script>