<?php include("../../connectionsettings.inc"); ?>
<?php include("../../include/topframe.php"); ?>
<title>User Master</title>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid " id="add">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>User Master</h1>
                </div>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-primary float-right" id="additem"><i class="fas fa-plus"></i> Add item</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="form" hidden>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">User Form</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="card-body">
                                <div class="form-group row" id="emp_row">
                                    <label for="emp_drp" class="col-sm-2 col-form-label">Employee</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2bs4" id="emp_drp" style="height:100%;width: 100%;">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="usernameid" class="col-sm-2 col-form-label">usernameid</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameid" placeholder="userid">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="passwd" class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="password" placeholder="password ..."></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="defaultlandingpage" class="col-sm-2 col-form-label">Defaultlandingpage</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="defaultlandingpage" placeholder="defaultlandingpage">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <button type="button" class="btn  btn-success float-right" onclick="save()">
                                            <i class="fas fa-save"></i> Save</button>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" id="cancel" class="btn btn-danger">
                                            <i class="fas fa-times-circle"></i> Cancel</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
        <div class="container-fluid" id="table">
            <div class="loaderclass" hidden>
            </div>
            <div class="divclass">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">User Details</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" id="usr_dt">
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    var userid = 0;
    $(document).ready(function() {

        $("#additem").click(function() {
            $("#table").hide();
            $("#emp_row").removeAttr('hidden');
            $("#form").show().removeAttr('hidden');
            $("#add").hide();
            clear();
            LoadComboboxFast();
            userid = 0;
        });
        $("#cancel").click(function() {
            $("#table").show();
            $("#form").hide();
            $("#add").show();
        });
        datatable();
        // LoadComboboxFast();
    });
    // -------------------------------------table---------------------------
    function datatable() {
        $("#add").show();
        $("#form").hide();
        $("#table").show();
        loaderstart("divclass", "loaderclass");
        $.ajax({
            type: "GET",
            url: "usermasterget.php",
            success: function(response) {
                $("#usr_dt").html(response);
                loaderend("divclass", "loaderclass");
            }
        })
    }
    // -------------------------------------Dropdown---------------------------
    function LoadComboboxFast() {
        $("#emp_drp").empty();
        $.ajax({
            type: "GET",
            url: "usermasterdetails.php",
            success: function(response) {
                $("#emp_drp").html(response);
                $('#emp_drp').select2().val(null).trigger("change");
            }
        })
    }
    //----------------------------------------save------------------------------------------------
    function save() {
        var emp_drp = $("#emp_drp").val();
        var usernameid = $("#usernameid").val();
        var password = $("#password").val();
        var defaultlandingpage = $("#defaultlandingpage").val();
        if (userid == 0) {
            if (emp_drp == "" || emp_drp == null) {
                Swal.fire("", "employee name must be filled out", 'warning')
                return false;
            }
        }
        if (usernameid == "" || usernameid == null) {
            Swal.fire("", "usernameid must be filled out", 'warning')
            return false;
        }
        if (password == "" || password == null) {
            Swal.fire("", "password must be filled out", 'warning')
            return false;
        }
        if (defaultlandingpage == "" || defaultlandingpage == null) {
            Swal.fire("", "defaultlandingpage must be filled out", 'warning')
            return false;
        }
        var type = "";
        if (userid == 0) {
            type = "save"
        } else {
            type = "update"
        }
        //console.log(type, designation_id, designation_name, remarks, priority);
        $.ajax({
            type: "POST",
            data: {
                data: userid,
                emp_drp_dt: emp_drp,
                usernameid_dt: usernameid,
                password_dt: password,
                defaultlandingpage_dt: defaultlandingpage,
                type: type
            },
            url: "usermasterset.php",
            success: function(response) {
                if (response == "0") {
                    statusswal('User created failed ', 'error');
                } else if (response == "1") {
                    statusswal('User created successfully', 'success');
                    datatable();
                    LoadComboboxFast();
                } else {
                    statusswal('please chosse another name,Name already taken', 'warning');
                }
            }
        })
    }
    //--------------------------------------EDIT------------------------------------//
    function editid(id, usernameid, password, defaultlandingpage) {
        userid = id;
        $("#emp_row").attr('hidden', 'hidden');
        $("#usernameid").val(usernameid);
        $("#password").val(password);
        $("#defaultlandingpage").val(defaultlandingpage);
        $("#table").hide();
        $("#form").show().removeAttr('hidden');
        $("#add").hide();
    }
    // -------------------------delete----------------------------------------
    // function removeemployee(id) {

    //     userid = id;
    //     var type = "";
    //     if (userid != "" && userid != null) {
    //         type = "delete"
    //     }
    //     Swal.fire({
    //         title: 'Are you sure?',
    //         text: "You won't be able to revert this!",
    //         icon: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, delete it!'
    //     }).then((result) => {
    //         if (result.isConfirmed) {
    //             $.ajax({
    //                 type: "POST",
    //                 data: {
    //                     "data": userid,
    //                     "type": type
    //                 },
    //                 url: "usermasterset.php",
    //                 success: function(response) {
    //                     if (response == "0") {
    //                         Swal.fire("", 'user  removed. failed', 'error')
    //                     } else {
    //                         Swal.fire("", 'user  has been removed.', 'success')
    //                         datatable();
    //                     }
    //                 }
    //             })
    //         }
    //     })
    // }
    // -------------------------end----------------------------------------
    function clear() {
        $("#emp_drp").val("");
        $("#usernameid").val("");
        $("#password").val("");
        $("#defaultlandingpage").val("");
    }
</script>
<?php include("../../include/footer.php"); ?>