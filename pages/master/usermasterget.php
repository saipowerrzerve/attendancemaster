<?php include("../../connectionsettings.inc"); ?>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>id_code</th>
            <th>usernameid</th>
            <th>Defaultlandingpage</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $qry = "SELECT  id_code,um.userid, CAST(AES_DECRYPT(passwd, 'passw') AS CHAR(50)) passwd 
        ,defaultlandingpage,usernameid
from usermaster um left join employee emp on emp.emp_id=um.emp_id order by um.userid";
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        if ($count == 0) {
        ?>
            <?php
        } else {
            while ($row = mysqli_fetch_array($result)) {
            ?>
                <tr>
                    <td><?php echo $row['id_code']; ?> </td>
                    <td><?php echo $row['usernameid']; ?></td>
                    <td><?php echo $row['defaultlandingpage']; ?></td>
                    <td>
                        <span>
                            <button class="btn btn-info btn-sm" onclick="editid('<?php echo $row['userid']; ?>',
                    '<?php echo $row['usernameid']; ?>',
                    '<?php echo $row['passwd']; ?>','<?php echo $row['defaultlandingpage']; ?>')">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Edit
                            </button>
                        </span>
                        <!-- <span>
                            <a alt="Delete" href="javascript:removeemployee(<?php echo $row['userid']; ?>)"><button class="btn btn-danger btn-sm">
                                    <i class="fas fa-trash">
                                    </i>
                                    Delete
                                </button></a>
                        </span> -->
                    </td>
                </tr>
            <?php } ?>
        <?php
        } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>