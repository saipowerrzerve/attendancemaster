<?php include("../../connectionsettings.inc");
$userid = $_GET['userid'];
?>
<table id="usr_tab" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>
                <!-- <input type="checkbox" id="chkselectall" onchange="Selectall()"> -->
                <div class="icheck-primary d-inline">
                    <input type="checkbox" id="chkselectall" onchange="Selectall()">
                    <label for="chkselectall">
                    </label>
                </div>
            </th>
            <th>Menu</th>
            <th>View</th>
            <th>Edit</th>
            <th>Add</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $qry = "SELECT `accessid`,mm.menuname
         ,mm.`menuid`, `viewdetails`, `editdetails`, `addmenu`, `deletemenu`
        FROM `menumaster` mm  left join userprivileges up on up.menuid=mm.menuid
        and up.userid=$userid  order by mm.menuid";
        $result = mysqli_query($dbh, $qry);
        $count = mysqli_num_rows($result);
        $counter = 1;
        if ($count == 0) {
        ?>
            <?php
        } else {

            while ($row = mysqli_fetch_array($result)) {
                $viewdetails = "";
                $editdetails = "";
                $addmenu = "";
                $deletemenu = "";
                if ($row['viewdetails'] == 1) {
                    $viewdetails = "checked";
                }
                if ($row['editdetails'] == 1) {
                    $editdetails = "checked";
                }
                if ($row['addmenu'] == 1) {
                    $addmenu = "checked";
                }
                if ($row['deletemenu'] == 1) {
                    $deletemenu = "checked";
                }
            ?>
                <tr>
                    <td><?php echo $counter; ?> </td>
                    <td><input type="checkbox" hidden id="m_<?php echo $row['menuid']; ?>" value="<?php echo $row['menuid']; ?>" class='checks' style="width: 100px;">
                        <span><?php echo $row['menuname']; ?></span>
                    </td>
                    <td>
                        <!-- <input type="checkbox" <?php echo $viewdetails ?> id="view_<?php echo $row['menuid']; ?>"> -->
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="view_<?php echo $row['menuid']; ?>" <?php echo $viewdetails ?>>
                            <label for="view_<?php echo $row['menuid']; ?>">
                            </label>
                        </div>
                    </td>
                    <td>
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="edit_<?php echo $row['menuid']; ?>" <?php echo $editdetails ?>>
                            <label for="edit_<?php echo $row['menuid']; ?>">
                            </label>
                        </div>
                    </td>
                    <td>
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="add_<?php echo $row['menuid']; ?>" <?php echo $addmenu ?>>
                            <label for="add_<?php echo $row['menuid']; ?>">
                            </label>
                        </div>
                    </td>
                    <td>
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="delete_<?php echo $row['menuid']; ?>" <?php echo $deletemenu ?>>
                            <label for="delete_<?php echo $row['menuid']; ?>">
                            </label>
                        </div>
                    </td>
                </tr>

            <?php
                $counter++;
            } ?>
        <?php
        } ?>
    </tbody>
</table>
<script>
    $(function() {
        $("#usr_tab").DataTable({
            "lengthMenu": [
                [-1],
                ["All"]
            ],
            "responsive": true,
            //"lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "print"],
        }).buttons().container().appendTo('#usr_tab_wrapper .col-md-6:eq(0)');
    });
//-------------------------------------------CHECK BOX Selectall---------------------------------
    function Selectall() {
        var chk = $("#chkselectall").is(":checked");
        var items = document.getElementsByClassName('checks');

        for (var i = 0; i < items.length; i++) {
            if (chk) {
                $('#view_' + items[i].value).prop('checked', true);
                $('#edit_' + items[i].value).prop('checked', true);
                $('#add_' + items[i].value).prop('checked', true);
                $('#delete_' + items[i].value).prop('checked', true);
            } else {
                $('#view_' + items[i].value).prop('checked', false);
                $('#edit_' + items[i].value).prop('checked', false);
                $('#add_' + items[i].value).prop('checked', false);
                $('#delete_' + items[i].value).prop('checked', false);
            }
        }
    }
</script>